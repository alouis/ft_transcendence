import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { UsersModule } from '../modules/user/user.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from '../infra/typeorm/entities/user.entity';
import { AuthModule } from '../modules/auth/auth.module';
import { TwoFactorsAuthenticationModule } from 'src/modules/2fa/2fa.module';
import { AuthMiddleware } from 'src/middlewares/auth/auth.middleware';
import { ChatModule } from 'src/modules/chat/chat.module';
import { SocketEntity } from 'src/infra/typeorm/entities/socket.entity';
import { FriendsModule } from 'src/modules/friends/friends.module';
import { FriendsEntity } from 'src/infra/typeorm/entities/friends.entity';
import { RoomEntity } from 'src/infra/typeorm/entities/room.entity';
import { UserRoomEntity } from 'src/infra/typeorm/entities/user_x_room.entity';
import { EntityManager } from 'typeorm';
import { MessageEntity } from 'src/infra/typeorm/entities/message.entity';
import { GameModule } from 'src/modules/game/game.module';
import { AdminRoomEntity } from 'src/infra/typeorm/entities/admin_x_room.entity';
import { MutedUserEntity } from 'src/infra/typeorm/entities/muted_user.entity';
import { BannedUserEntity } from 'src/infra/typeorm/entities/banned_user.entity';
import { GameEntity } from 'src/infra/typeorm/entities/game.entity';
import { ScoresEntity } from 'src/infra/typeorm/entities/scores.entity';
import { BlockedUserEntity } from 'src/infra/typeorm/entities/blocked_user.entity';
import { MulterModule } from '@nestjs/platform-express';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

@Module({
    imports: [
        TypeOrmModule.forRoot({
            type: 'postgres',
            host: process.env.TRANS_ENV === 'prod' ? 'database' : 'localhost',
            port: 5432,
            username: 'trans_user',
            password: 'trans_password',
            database: 'trans_db',
            entities: [
                UserEntity,
                SocketEntity,
                FriendsEntity,
                RoomEntity,
                UserRoomEntity,
                MessageEntity,
                AdminRoomEntity,
                MutedUserEntity,
                BannedUserEntity,
                GameEntity,
                ScoresEntity,
                BlockedUserEntity,
            ],
            synchronize: false,
        }),
        UsersModule,
        AuthModule,
        FriendsModule,
        ChatModule,
        TwoFactorsAuthenticationModule,
        GameModule,
        MulterModule.register({
            dest: './avatars',
        }),
        ServeStaticModule.forRoot({
            serveRoot: '/avatars',
            rootPath: join(__dirname, '../../avatars'),
            serveStaticOptions: {
                fallthrough: false,
                index: false,
            },
        }),
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule implements NestModule {
    constructor(private entityManager: EntityManager) {}
    configure(consumer: MiddlewareConsumer) {
        consumer.apply(AuthMiddleware).forRoutes('users');
        consumer.apply(AuthMiddleware).forRoutes('friends');
        consumer.apply(AuthMiddleware).forRoutes('chat');
        consumer.apply(AuthMiddleware).forRoutes('game');
    }
}
