export class UserSecretCodeDto {
    otpauthUrl: string;
    secret: string;
}
