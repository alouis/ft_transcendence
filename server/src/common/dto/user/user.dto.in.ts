import { IsString, IsNotEmpty } from 'class-validator';

export class CreateUserDto {
    @IsString()
    @IsNotEmpty()
    login: string;

    @IsString()
    username: string;

    @IsString()
    twoFactorAuthenticationSecret: string;

    avatar: string;
}

export class MuteUserDto {
    userId: number;
    roomId: number;
    time: string;
}
