import { IsNumber, IsString } from 'class-validator';

export class socketAndUserIdsDto {
    @IsString()
    socketId: string;

    @IsNumber()
    userId: number;
}
