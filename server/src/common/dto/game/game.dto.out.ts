import { IsNumber, IsString } from 'class-validator';

export class MatchResults {
    @IsNumber()
    ownScore: number;

    @IsNumber()
    opponentScore: number;

    @IsString()
    opponentUsername: string;

    @IsString()
    opponentAvatar: string;
}
