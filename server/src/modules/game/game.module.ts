import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '../user/user.module';
import { GameGateway } from './game.gateway';
import { GameService } from './game.service';
import { JwtModule } from '@nestjs/jwt';
import { GameEntity } from 'src/infra/typeorm/entities/game.entity';
import { GameController } from './game.controller';
import { GameEngine } from './GameEngine';
import { ScoresEntity } from 'src/infra/typeorm/entities/scores.entity';
import { ChatModule } from '../chat/chat.module';

@Module({
    controllers: [GameController],
    providers: [GameGateway, GameService, GameEngine],
    imports: [
        TypeOrmModule.forFeature([GameEntity, ScoresEntity]),
        forwardRef(() => UsersModule),
        ChatModule,
        JwtModule.register({ secret: process.env.TRANS_JWT_SECRET }),
    ],
    exports: [GameService],
})
export class GameModule {}
