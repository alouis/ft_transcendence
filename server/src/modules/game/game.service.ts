import { forwardRef, Inject, Injectable, Logger } from '@nestjs/common';
import { GameEngine } from './GameEngine';
import {
    Attendee,
    GameMessage,
    GameSettings,
    GameStatus,
    GameUpdate,
    MoveDirection,
    Role,
    TickMessage,
} from 'src/common/types/game.globals.types';
import { InjectRepository } from '@nestjs/typeorm';
import { Not, Repository } from 'typeorm';
import { GameEntity } from 'src/infra/typeorm/entities/game.entity';
import { GameMode } from './game.types';
import { GameGateway } from './game.gateway';
import { ModuleRef } from '@nestjs/core';
import { UsersService } from '../user/user.service';
import { UserFromDBDto } from 'src/common/dto/user/user.dto.out';
import { ScoresEntity } from 'src/infra/typeorm/entities/scores.entity';
import { ChatService } from '../chat/chat.service';

@Injectable()
export class GameService {
    private readonly logger;
    gameEngines: Map<number, GameEngine>;

    constructor(
        @Inject(forwardRef(() => GameGateway))
        private readonly gameGateway: GameGateway,
        private readonly usersService: UsersService,
        private readonly chatService: ChatService,
        @InjectRepository(GameEntity)
        private gameRepository: Repository<GameEntity>,
        @InjectRepository(ScoresEntity)
        private scoresRepository: Repository<ScoresEntity>,
        private moduleRef: ModuleRef,
    ) {
        this.logger = new Logger(GameService.name);
        this.gameEngines = new Map<number, GameEngine>();
    }

    async saveGame(gameUpdate: GameUpdate) {
        await this.gameRepository.save({
            id: gameUpdate.id,
            player1Id: gameUpdate.player1?.userId,
            player2Id: gameUpdate.player2?.userId,
            score1: gameUpdate.score1,
            score2: gameUpdate.score2,
            status: gameUpdate.status,
        });
    }
    async getPlayerResume(
        playerId: number,
    ): Promise<{ victories: string; losses: string; equalities: string } | undefined> {
        const resume = await this.gameRepository.query(`
		with victories as (
			select count(*) from games where (("player1Id" = ${playerId} and score1 > score2) or ("player2Id" = ${playerId} and score2 > score1)) and score1 is not null and score2 is not null and status = 'ENDED'
		),
		losses as (
			select count(*) from games where (("player1Id" = ${playerId} and score1 < score2) or ("player2Id" = ${playerId} and score2 < score1)) and score1 is not null and score2 is not null and status = 'ENDED'
		),
		draws as (
			select count(*) from games where ("player1Id" = ${playerId} or "player2Id" = ${playerId}) and score2 = score1 and score1 is not null and score2 is not null and status = 'ENDED' and not (score1 = -1 and score2 = -1)
		)
		select victories."count" as victories, losses."count" as losses, draws."count" as equalities from victories, losses, draws`);
        return resume?.[0];
    }
    async savePlayerResults(playerId: number) {
        const playerResume = await this.getPlayerResume(playerId);
        const scoreId = (await this.scoresRepository.findOne({ playerId }))?.id;
        const playerScore = {
            victories: parseInt(playerResume.victories),
            losses: parseInt(playerResume.losses),
            equalities: parseInt(playerResume.equalities),
            playerId,
            id: scoreId ?? undefined,
            winningRatio: parseInt(playerResume.victories) * 3 + parseInt(playerResume.equalities),
        };
        await this.scoresRepository.save(playerScore);
    }

    async createNewGame(userId: number, settings: GameSettings): Promise<{ gameId: number; roomId: string }> {
        const game = await this.gameRepository.insert({ player1Id: userId });
        const gameId = game.identifiers[0].id;
        const gameEngine = await this.moduleRef.create(GameEngine);
        gameEngine.init({ gameId, settings });
        this.gameEngines.set(gameId, gameEngine);
        await gameEngine.addAttendee(userId);
        gameEngine.start();
        await this.chatService.notifyFriendsOfConnection(userId);
        this.logger.log(`UserId: ${userId} created new game ${gameId}`);
        this.logger.log(`${this.gameEngines.size} games running`);
        return { gameId, roomId: this.getRoomId(gameId) };
    }

    async setupTrainingGame(
        userId: number,
        mode: 'INCREMENTAL' | 'REAL_TIME',
    ): Promise<{ gameId: number; roomId: string }> {
        const game = await this.gameRepository.insert({ player1Id: null, player2Id: userId });
        const gameId = game.identifiers[0].id;
        const gameEngine = await this.moduleRef.create(GameEngine);
        gameEngine.init({ gameId, mode: GameMode.TRAINING });
        this.gameEngines.set(gameId, gameEngine);
        gameEngine.addTrainer(userId);
        if (mode == 'REAL_TIME') gameEngine.start();
        this.logger.log(`UserId: ${userId} created a new training game ${gameId}`);
        this.logger.log(`${this.gameEngines.size} games running`);
        return { gameId, roomId: this.getRoomId(gameId) };
    }
    async joinGame(gameId: number, userId: number): Promise<{ gameId: number; roomId: string }> {
        const gameEngine = gameId
            ? this.gameEngines.get(gameId)
            : (this.gameEngines.values().next().value as GameEngine);
        if (!gameEngine) return null;
        if (gameEngine.getAttendeeFromId(userId) === null) {
            const newAttendee = await gameEngine.addAttendee(userId);
            if (newAttendee.role !== Role.VIEWER) {
                await this.chatService.notifyFriendsOfConnection(userId);
            }
        }
        this.logger.log(`UserId: ${userId} joined game ${gameEngine.gameId} with role ${gameEngine.getRole(userId)}`);
        return { gameId: gameEngine.gameId, roomId: this.getRoomId(gameId) };
    }
    async stopGame(gameId: number) {
        if (!this.gameEngines.has(gameId)) return;
        const gameEngine = this.gameEngines.get(gameId);
        if (gameEngine.status != GameStatus.ENDED) gameEngine.stop();
        if ((await this.gameRepository.findOne(gameId)).player2Id === null) {
            this.gameRepository.delete({ id: gameId });
        } else {
            const player1Id = (await this.gameRepository.findOne(gameId)).player1Id;
            const player2Id = (await this.gameRepository.findOne(gameId)).player2Id;
            await this.savePlayerResults(player1Id);
            await this.savePlayerResults(player2Id);
            await this.chatService.notifyFriendsOfDisconnection(player1Id);
            await this.chatService.notifyFriendsOfDisconnection(player2Id);
        }
        this.gameEngines.delete(gameId);
        this.logger.log(`Game ${gameId} killed. ${this.gameEngines.size} games running`);
    }

    leaveAllGames(userId: number) {
        // TODO: dirty
        this.gameEngines.forEach((gameEngine) => {
            this.leaveGame(gameEngine.gameId, userId);
        });
    }

    async leaveGame(gameId: number, userId: number) {
        if (!this.gameEngines.has(gameId)) return;
        const gameEngine = this.gameEngines.get(gameId);
        try {
            const leftAttendee = await gameEngine.removeAttendee(userId);
            if (leftAttendee.role != Role.VIEWER) await this.chatService.notifyFriendsOfDisconnection(userId);
            this.logger.log(`User ${userId} left game ${gameEngine.gameId}`);
            if (gameEngine.getStatus() == GameStatus.ENDED) this.stopGame(gameId);
        } catch (e) {
            return;
        }
    }

    getRoomId(gameId: number) {
        return `room ${gameId}`;
    }
    getScore(gameId: number) {
        return this.gameEngines.get(gameId)?.getScore();
    }
    getSettings(gameId: number) {
        return this.gameEngines.get(gameId)?.getSettings();
    }

    getGameUpdate(gameId: number) {
        return this.gameEngines.get(gameId)?.getGameUpdate();
    }
    async getAttendees(gameId: number): Promise<UserFromDBDto[]> {
        const attendees = this.gameEngines.get(gameId)?.attendees;
        if (!attendees?.size) return [];
        const users = await Promise.all(
            Array.from(attendees?.keys()).map((userId) => {
                return this.usersService.findById(userId);
            }),
        );
        return users;
    }
    async getPlayers(gameId: number): Promise<UserFromDBDto[]> {
        if (!gameId) return [];
        const game = await this.gameRepository.findOne({ select: ['player1Id', 'player2Id'], where: { id: gameId } });
        if (!game) return [];
        const player1 = game.player1Id ? await this.usersService.findById(game.player1Id) : null;
        const player2 = game.player2Id ? await this.usersService.findById(game.player2Id) : null;
        return [player1, player2];
    }

    move(gameId: number, userId: number, direction: MoveDirection) {
        this.gameEngines.get(gameId)?.movePlayer(userId, direction);
    }
    launchBall(gameId: number, userId: number) {
        this.gameEngines.get(gameId)?.launchBall(userId);
    }
    step(gameId: number, stepInterval: number) {
        return this.gameEngines.get(gameId).step(stepInterval);
    }

    async findGamesForUser(userId: number): Promise<GameEntity[] | null> {
        const games = await this.gameRepository.find({
            where: [
                { player1Id: userId, status: 'ENDED' },
                { player2Id: userId, status: 'ENDED' },
            ],
            order: { id: 'DESC' },
        });
        return games.length === 0 ? null : games;
    }
    async getCurrentGames(): Promise<GameEntity[]> {
        return this.gameRepository.find({ where: { status: Not(GameStatus.ENDED) }, order: { id: 'DESC' }, take: 10 });
    }
    async sendGameUpdate(gameId: number, update: GameUpdate) {
        const attendees = await this.getAttendees(gameId);
        this.gameGateway.emitEvent(this.getRoomId(gameId), GameMessage.UPDATE, { update, attendees });
    }

    sendAttendeeEvent(gameId: number, event: 'arrived' | 'left', attendee: Attendee) {
        const eventMessage = event === 'arrived' ? GameMessage.ATTENDEE_ARRIVED : GameMessage.ATTENDEE_LEFT;
        this.gameGateway.emitEvent(this.getRoomId(gameId), eventMessage, attendee);
    }

    sendTick(gameId: number, tickMessage: TickMessage) {
        this.gameGateway.emitEvent(this.getRoomId(gameId), GameMessage.GAME_TICK, tickMessage);
    }
}
