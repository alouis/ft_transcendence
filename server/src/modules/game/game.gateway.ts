import { forwardRef, Inject, Injectable, Logger } from '@nestjs/common';
import { ConnectedSocket, MessageBody, SubscribeMessage, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Socket, Server } from 'socket.io';
import { GAME_SOCKET_PORT } from 'src/common/network';
import { GameMap, GameMessage, MoveDirection } from 'src/common/types/game.globals.types';
import { UsersService } from '../user/user.service';
import { GameService } from './game.service';

@WebSocketGateway(GAME_SOCKET_PORT, { cors: true })
@Injectable()
export class GameGateway {
    private readonly logger;
    @WebSocketServer() server: Server;

    constructor(
        @Inject(forwardRef(() => GameService))
        private readonly gameService: GameService,
        private readonly userService: UsersService,
    ) {
        this.logger = new Logger(GameService.name);
    }

    @SubscribeMessage(GameMessage.CREATE_GAME)
    async createGame(
        @MessageBody('level') level: number,
        @MessageBody('map') map: GameMap,
        @MessageBody('powerups') powerups: boolean,
        @ConnectedSocket() socket: Socket,
    ) {
        const token = socket.handshake.auth.token || socket.handshake.query.token; // to enable test with postman
        const userId = await this.userService.getIdFromValidJWT(token);
        const { gameId, roomId } = await this.gameService.createNewGame(userId, { level, map, powerups });
        if (roomId) socket.join(roomId);
        return { gameId };
    }
    @SubscribeMessage(GameMessage.JOIN_GAME)
    async joinGame(@MessageBody('gameId') gameId: string, @ConnectedSocket() socket: Socket) {
        const token = socket.handshake.auth.token || socket.handshake.query.token; // to enable test with postman
        const userId = await this.userService.getIdFromValidJWT(token);
        const ret = await this.gameService.joinGame(parseInt(gameId), userId);
        if (ret) {
            socket.join(ret.roomId);
            return ret;
        }
        return { error: 'no game' };
    }
    @SubscribeMessage(GameMessage.SETUP_TRAINING_GAME)
    async setupTrainingGame(@ConnectedSocket() socket: Socket) {
        const token = socket.handshake.auth.token || socket.handshake.query.token; // to enable test with postman
        const userId = await this.userService.getIdFromValidJWT(token);
        const { gameId, roomId } = await this.gameService.setupTrainingGame(userId, 'REAL_TIME');
        if (roomId) socket.join(roomId);
        return { gameId };
    }
    @SubscribeMessage(GameMessage.SETUP_AI_TRAINING)
    async setupAIAgent(@ConnectedSocket() socket: Socket) {
        const AIUserId = 0;
        const { gameId, roomId } = await this.gameService.setupTrainingGame(AIUserId, 'INCREMENTAL');
        if (roomId) socket.join(roomId);
        this.gameService.launchBall(gameId, AIUserId);
        return { gameId };
    }

    @SubscribeMessage(GameMessage.LEAVE_GAME)
    async leaveGame(@MessageBody('gameId') gameId: number, @ConnectedSocket() socket: Socket) {
        const roomId = this.gameService.getRoomId(gameId);
        const token = socket.handshake.auth.token || socket.handshake.query.token; // to enable test with postman
        const userId = await this.userService.getIdFromValidJWT(token);
        await this.gameService.leaveGame(gameId, userId);
        socket.leave(roomId);
    }

    @SubscribeMessage(GameMessage.MOVE)
    async move(
        @MessageBody('gameId') gameId: number,
        @MessageBody('direction') direction: MoveDirection,
        @ConnectedSocket() socket: Socket,
    ) {
        const token = socket.handshake.auth.token || socket.handshake.query.token; // to enable test with postman
        const userId = await this.userService.getIdFromValidJWT(token);
        this.gameService.move(gameId, userId, direction);
    }
    @SubscribeMessage(GameMessage.LAUNCH_BALL)
    async launchBall(@MessageBody('gameId') gameId: number, @ConnectedSocket() socket: Socket) {
        const token = socket.handshake.auth.token || socket.handshake.query.token; // to enable test with postman
        const userId = await this.userService.getIdFromValidJWT(token);
        this.gameService.launchBall(gameId, userId);
    }

    async emitEvent(roomId: string, event: string, payload: unknown) {
        this.server.to(roomId).emit(event, payload);
    }

    async handleDisconnect(socket: Socket) {
        const token = socket.handshake.auth.token || socket.handshake.query.token; // to enable test with postman
        const userId = await this.userService.getIdFromValidJWT(token);
        this.gameService.leaveAllGames(userId);
        this.logger.log(`Game client disconnected: ${socket.id}`);
        return;
    }

    async handleConnection(socket: Socket) {
        this.logger.log(`Game client connected: ${socket.id}`);
        return;
    }
}
