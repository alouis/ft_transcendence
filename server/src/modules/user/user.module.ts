import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersController } from './user.controller';
import { UsersService } from './user.service';
import { UserEntity } from '../../infra/typeorm/entities/user.entity';
import { JwtModule } from '@nestjs/jwt';
import { SocketEntity } from 'src/infra/typeorm/entities/socket.entity';
import { MutedUserEntity } from 'src/infra/typeorm/entities/muted_user.entity';
import { BannedUserEntity } from 'src/infra/typeorm/entities/banned_user.entity';
import { ScoresEntity } from 'src/infra/typeorm/entities/scores.entity';
import { BlockedUserEntity } from 'src/infra/typeorm/entities/blocked_user.entity';
import { GameModule } from '../game/game.module';
import { GameEntity } from 'src/infra/typeorm/entities/game.entity';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            UserEntity,
            SocketEntity,
            MutedUserEntity,
            BannedUserEntity,
            ScoresEntity,
            BlockedUserEntity,
            GameEntity,
        ]),
        JwtModule.register({ secret: process.env.TRANS_JWT_SECRET }),
        forwardRef(() => GameModule),
    ],
    controllers: [UsersController],
    providers: [UsersService],
    exports: [UsersService],
})
export class UsersModule {}
