import { Controller, Get, HttpException, HttpStatus, Param, Post } from '@nestjs/common';
import { TwoFactorsAuthenticationService } from './2fa.service';
import { UsersService } from '../user/user.service';

@Controller('2fa')
export class TwoFactorsAuthenticationController {
    constructor(private readonly twoFAService: TwoFactorsAuthenticationService, private usersService: UsersService) {}

    @Post('generate/store/:login')
    async generateAndStoreTwoFactorsAuthentication(@Param() params: { login: string }) {
        if ((await this.usersService.is2FASecretCreated(params.login)) === false) {
            const factors = await this.twoFAService.generate2FASecret(params.login);
            await this.usersService.set2FASecret(params.login, factors.secret);
            return factors.otpauthUrl;
        }
        throw new HttpException('Secret already generated', HttpStatus.FORBIDDEN);
    }

    @Post('validate/:login/:code')
    async validateCodeAgainstSecret(@Param() params: { login: string; code: string }): Promise<boolean> {
        const user = await this.usersService.returnUserEntity(params.login);
        return this.twoFAService.is2FACodeValid(params.code, user);
    }

    @Post('disable/:login')
    async remove2FA(@Param() params: { login: string }) {
        if (await this.usersService.is2FASecretCreated(params.login)) {
            await this.usersService.remove2FASecret(params.login);
        }
    }

    @Get('enabled/:login')
    async is2FAEnabled(@Param() params: { login: string }): Promise<boolean> {
        const res = await this.usersService.is2FASecretCreated(params.login);
        return res;
    }
}
