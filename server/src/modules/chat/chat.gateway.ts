import {
    ConnectedSocket,
    MessageBody,
    OnGatewayConnection,
    OnGatewayDisconnect,
    OnGatewayInit,
    SubscribeMessage,
    WebSocketGateway,
    WebSocketServer,
} from '@nestjs/websockets';
import { Socket, Server } from 'socket.io';
import { ChatService } from './chat.service';
import { UsersService } from '../user/user.service';
import { forwardRef, Inject, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BlockedUserEntity } from 'src/infra/typeorm/entities/blocked_user.entity';
import { Repository } from 'typeorm';
import { CHAT_SOCKET_PORT } from 'src/common/network';

@WebSocketGateway(CHAT_SOCKET_PORT, { cors: true })
export class ChatGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
    private readonly logger;
    @WebSocketServer() server: Server;
    constructor(
        private readonly usersService: UsersService,
        @Inject(forwardRef(() => ChatService))
        private chatService: ChatService,
        @InjectRepository(BlockedUserEntity)
        private blockedUsersRepository: Repository<BlockedUserEntity>,
    ) {
        this.logger = new Logger(ChatGateway.name);
    }

    @SubscribeMessage('events')
    async handleEvent(
        @MessageBody('roomId') roomId: number,
        @MessageBody('message') content: string,
        @ConnectedSocket() socket: Socket,
    ): Promise<string> {
        const token = socket.handshake.auth.token || socket.handshake.query.token; // to enable test with postman
        const login = await this.usersService.getLoginFromValidJWT(token);
        const senderId = await this.usersService.findIdByLogin(login);
        if ((await this.usersService.isUserStillMutedForRoom(senderId, roomId)) === false) {
            const time = new Date().toString();
            await this.chatService.storeMessageInDB({ senderId, roomId, time, content });
            this.server.in(roomId.toString()).emit('events', { senderId, roomId, time, content });
        }
        return content;
    }

    @SubscribeMessage('invite')
    async handleInvite(
        @MessageBody('roomId') roomId: number,
        @MessageBody('gameId') gameId: number,
        @ConnectedSocket() socket: Socket,
    ): Promise<void> {
        const token = socket.handshake.auth.token || socket.handshake.query.token; // to enable test with postman
        const senderId = await this.usersService.getIdFromValidJWT(token);
        const username = await this.usersService.findUsernameById(senderId);
        const message = `${username} invite you to play`;

        if ((await this.usersService.isUserStillMutedForRoom(senderId, roomId)) === false) {
            socket.to(roomId.toString()).emit('invite', { message, gameId });
        }
    }

    @SubscribeMessage('leave-room')
    async leaveRoom(@MessageBody('roomId') roomId: number, @ConnectedSocket() socket: Socket): Promise<boolean> {
        const token = socket.handshake.auth.token || socket.handshake.query.token; // to enable test with postman
        const login = await this.usersService.getLoginFromValidJWT(token);
        const senderId = await this.usersService.findIdByLogin(login);
        if ((await this.chatService.isUserInRoom(senderId, roomId)) === false) socket.leave(roomId.toString());
        return true;
    }

    @SubscribeMessage('join-room')
    async joinRoom(@MessageBody('roomId') roomId: number, @ConnectedSocket() socket: Socket): Promise<boolean> {
        const token = socket.handshake.auth.token || socket.handshake.query.token; // to enable test with postman
        const login = await this.usersService.getLoginFromValidJWT(token);
        const senderId = await this.usersService.findIdByLogin(login);
        if ((await this.chatService.isUserInRoom(senderId, roomId)) === true) socket.join(roomId.toString());
        return true;
    }

    @SubscribeMessage('block-user')
    async blockUser(
        @MessageBody('blockedUserId') blockedUserId: number,
        @ConnectedSocket() socket: Socket,
    ): Promise<boolean> {
        const token = socket.handshake.auth.token || socket.handshake.query.token; // to enable test with postman
        const login = await this.usersService.getLoginFromValidJWT(token);
        const blockedById = await this.usersService.findIdByLogin(login);
        if ((await this.usersService.isUserBlocked(blockedUserId, blockedById)) === false) {
            await this.blockedUsersRepository.insert({ blockedUserId, blockedById });
            return true;
        }
        return false;
    }

    @SubscribeMessage('unblock-user')
    async unblockUser(
        @MessageBody('blockedUserId') blockedUserId: number,
        @ConnectedSocket() socket: Socket,
    ): Promise<boolean> {
        const token = socket.handshake.auth.token || socket.handshake.query.token; // to enable test with postman
        const login = await this.usersService.getLoginFromValidJWT(token);
        const blockedById = await this.usersService.findIdByLogin(login);
        if ((await this.usersService.isUserBlocked(blockedUserId, blockedById)) === true) {
            await this.blockedUsersRepository.delete({ blockedUserId, blockedById });
            return true;
        }
        return false;
    }

    afterInit() {
        this.logger.log('Init');
    }

    @SubscribeMessage('join-all-rooms')
    async joinAllRooms(client: Socket) {
        const token = client.handshake.auth.token || client.handshake.query.token; // to enable test with postman
        const login = await this.usersService.getLoginFromValidJWT(token);
        const userId = await this.usersService.findIdByLogin(login);
        await this.chatService.joinAllRooms(client, userId);
    }

    async handleDisconnect(client: Socket) {
        this.logger.log(`Chat client disconnected: ${client.id}`);
        const token = client.handshake.auth.token || client.handshake.query.token; // to enable test with postman
        const login = await this.usersService.getLoginFromValidJWT(token);
        const userId = await this.usersService.findIdByLogin(login);
        await this.chatService.leaveAllRooms(client, userId);
        await this.chatService.deleteSocketsForUser(userId);
        await this.chatService.notifyFriendsOfDisconnection(userId);
    }

    async handleConnection(client: Socket) {
        this.logger.log(`Chat client connected: ${client.id}`);
        const token = client.handshake.auth.token || client.handshake.query.token; // to enable test with postman
        const login = await this.usersService.getLoginFromValidJWT(token);
        const userId = await this.usersService.findIdByLogin(login);
        if ((await this.chatService.isUserStored(userId)) === true)
            // to be replaced to handle the case when user is connected on multiple equipments
            await this.chatService.deleteSocketsForUser(userId);
        await this.chatService.storeSocketAndUserId(client.id, userId);
        await this.chatService.joinAllRooms(client, userId);
        await this.chatService.notifyFriendsOfConnection(userId);
    }
}
