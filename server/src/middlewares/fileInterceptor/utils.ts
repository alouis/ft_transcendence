import { Request } from 'express';
import { v4 as uuidv4 } from 'uuid';

export const editFileName = (
    req: Request,
    file: Express.Multer.File,
    callback: (error: Error | null, filename: string) => void,
) => {
    const newUuid = uuidv4();
    const filename = req.context.login;
    const fileExtension = file.originalname.split('.').pop();
    callback(null, `${filename}${newUuid.substring(0, 3)}.${fileExtension}`);
};

export const imageFileFilter = (
    req: Request,
    file: Express.Multer.File,
    callback: (error: Error, acceptFile: boolean) => void,
) => {
    if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
        return callback(null, false);
    }
    callback(null, true);
};
