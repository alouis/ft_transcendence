import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { IsNumber } from 'class-validator';

@Entity('blocked_users')
export class BlockedUserEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @IsNumber()
    blockedUserId: number;

    @Column()
    @IsNumber()
    blockedById: number;
}
