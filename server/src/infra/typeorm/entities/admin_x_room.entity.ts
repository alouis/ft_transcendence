import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { IsNumber } from 'class-validator';

@Entity('admins_x_rooms')
export class AdminRoomEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @IsNumber()
    adminId: number;

    @Column()
    @IsNumber()
    roomId: number;
}
