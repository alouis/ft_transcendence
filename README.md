# Ft_transcendence

## How to use it :satellite:

- Clone the repository

#### Production mode

- Install Docker Desktop

- Run Docker

```
docker-compose up --build
```

- Visit http://localhost:3001

:warning: Private credentials are required to sign in with 42. If member of 42 school, it's possible to create an app to get the credentials needed for 42 api requests. Other than that, logging in is possible only if an account was already created (existing accounts include alouis, mroux, and bboop) :warning:

:warning: use  
```
docker-compose down
```
to exit properly :warning:   
  
  
#### Development mode

- Set the database up

```
cd ./database
./rebuildDatabase.sh trans_db_dump.sql

// to share your own version of the db
pg_dump db_name > /database/file_name
```

- Install packages at project's root

```
npm install
```

- Run server and client in folders associated (server first otherwise client will run on server's port)

```
../ft_transcendence/server$ npm run start:dev
../ft_transcendence/client$ npm run start

```

## Errors :wrench:

- Use form for user input to avoid rerendering the component when hitting 'Enter'
- With NestJS, order of the routes declared in controller matters. Start with dynamic routes then routes with params.
- serviceWorker from PWA can fire networks errors, to disable PWA in create-react-app : https://stackoverflow.com/questions/54644926/how-to-create-cra-project-without-pwa-features

## Git :bulb:

```
// see branches you have locally
git branch

// create new branch
git checkout -b <name of new branch>

// push on your branch
git push origin <name of your branch>

// switch branch
git checkout <name of the branch>

// see the differences between a branch locally and the same branch on the remote repository
git diff

// git pull only if merging is not required
git pull --ff-only

// checkout all the modifications you didn't commit
git checkout -- .

// save local modifications for later while checking them out
git stash

// push what you stashed
git stash apply

// recover/delete locally what you stashed
git stash pop
git stash drop

// restore file to previous version
git restore <file>

// delete your last commit (pushed)
git reset HEAD~1

// see the previous commits
git log

// same with colors
git log --all --color --graph --pretty=format:`%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cd) %C(bold blue)<%an>%Creset` --abbrev-commit

// switch branch to the one you were previously
git checkout -

// delete branch locally
git branch -d localBranchName

// delete branch remotely
git push origin --delete remoteBranchName

// rename branch
git branch -m <old> <new>

// find out who did what
git blame -L 48,58 <filename>


// see names of files with differences
git diff origin/main --name-only
```
