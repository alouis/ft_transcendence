import { Box, CircularProgress } from '@mui/material';
import React, { useContext } from 'react';
import { Navigate } from 'react-router-dom';
import { AuthContext } from '../contexts/auth.context';

const RequireAuth = ({ children }: { children: JSX.Element }) => {
    const authContext = useContext(AuthContext);

    if (authContext.isLoggedIn === null)
        return (
            <Box
                sx={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: '100vh',
                }}
            >
                <CircularProgress />
            </Box>
        );
    return authContext.isLoggedIn ? children : <Navigate to="/" />;
};

export default RequireAuth;
