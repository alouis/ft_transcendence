import React, { useContext } from 'react';
import { AuthContext } from '../contexts/auth.context';
import { Navigate } from 'react-router-dom';
import { Box, CircularProgress } from '@mui/material';

const RequireLogOut = ({ children }: { children: JSX.Element }) => {
    const authContext = useContext(AuthContext);

    const route = authContext.isUserNew === true ? '/settings' : '/profile';

    if (authContext.isLoggedIn === null)
        return (
            <Box
                sx={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: '100vh',
                }}
            >
                <CircularProgress />
            </Box>
        );
    else return authContext.isLoggedIn ? <Navigate to={route} /> : children;
};

export default RequireLogOut;
