import { ListItem, ListItemText } from '@mui/material';
import React from 'react';
import { MessageInConversation } from '../../common/chat/dto/messages.dto.in';

interface MessageReceivedProps {
    message: MessageInConversation;
}
const MessageReceived = (props: MessageReceivedProps) => {
    return (
        <ListItem
            sx={{
                backgroundColor: 'lightgray',
                marginBottom: 2,
                p: 1,
                width: 'auto',
                borderBottomRightRadius: 20,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
            }}
        >
            <ListItemText primary={props.message.content} />
        </ListItem>
    );
};

export default MessageReceived;
