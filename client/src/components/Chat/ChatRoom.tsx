import { Avatar, Box, Card, FormControl, IconButton, List, Menu, OutlinedInput, Typography } from '@mui/material';
import React, { useState, KeyboardEvent, useContext } from 'react';
import { sendMessage } from '../../api/socketIo/chatMessages';
import MoreVertRoundedIcon from '@mui/icons-material/MoreVertRounded';
import { RoomOverviewDto } from '../../common/chat/dto/room.dto.out';
import { MessageInConversation } from '../../common/chat/dto/messages.dto.in';
import AvatarWithOnlineBadge from '../AvatarWithOnlineBadge';
import { User } from '../../common/user/dto/user.dto.out';
import { AuthContext } from '../../contexts/auth.context';
import MessageReceived from './MessageReceived';
import MessageSent from './MessageSent';
import { ChatRoomOptionsList } from './ChatOptions/ChatOptions';
import ConversationNotification from './ConversationNotification';
import GroupsRoundedIcon from '@mui/icons-material/GroupsRounded';
import { RoomTypeEnum } from '../../common/chat/types/rooms.types';

export interface ChatRoomProps {
    selectedChatRoom: RoomOverviewDto;
    setSelectedChatRoom: (newRoom: RoomOverviewDto) => void;
    convMessages: MessageInConversation[];
    currentInterlocutors: User[];
    fetchRooms: (selectedRoom: number) => void;
}

const ChatRoom = (props: ChatRoomProps): JSX.Element => {
    const [messageTyped, setMessageTyped] = useState<string>('');
    const auth = useContext(AuthContext);
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

    const inputDisabled =
        props?.selectedChatRoom === undefined ||
        props?.selectedChatRoom?.mutedIds?.find((muteUserId) => muteUserId === auth.userId) !== undefined ||
        props?.selectedChatRoom?.roomId === 0;

    const openRoomSettings = (event: React.MouseEvent<HTMLElement>) => {
        if (props.selectedChatRoom !== undefined) setAnchorEl(event.currentTarget);
    };

    const closeRoomSettings = () => {
        setAnchorEl(null);
    };

    const handleKeyDown = async (event: KeyboardEvent<HTMLInputElement>): Promise<void> => {
        if (event.key === 'Enter' && props.selectedChatRoom !== undefined) {
            sendMessage(props.selectedChatRoom?.roomId, messageTyped);
            setMessageTyped('');
        }
    };

    return (
        <Card
            sx={{
                display: 'flex',
                flex: 1,
                flexGrow: 1,
                marginLeft: 1,
                borderRadius: 2,
                flexDirection: 'column',
                maxHeight: '90vh',
                overflow: 'hidden',
            }}
        >
            {/* 1st div -> avatar and name of friend/channel */}
            <Box
                sx={{
                    display: 'flex',
                    flexGrow: 1,
                    flexDirection: 'row',
                }}
            >
                {props.currentInterlocutors.length === 1 && props.selectedChatRoom.roomId !== 0 && (
                    <Box
                        sx={{
                            display: 'flex',
                            flexGrow: 1,
                            alignItems: 'center',
                        }}
                    >
                        {props.selectedChatRoom.type === RoomTypeEnum.DM ? (
                            <AvatarWithOnlineBadge
                                image={props.currentInterlocutors[0].avatar}
                                online={props.currentInterlocutors[0].isActive}
                                playing={props.currentInterlocutors[0].isPlaying}
                                sx={{ width: 40, height: 40, m: 2 }}
                            />
                        ) : (
                            <GroupsRoundedIcon sx={{ color: 'gray', m: 2, marginLeft: 3 }} />
                        )}
                        <Typography variant="h6" color="inherit">
                            {props.selectedChatRoom.name}
                        </Typography>
                    </Box>
                )}
                {props.selectedChatRoom !== undefined &&
                    props.selectedChatRoom.roomId !== 0 &&
                    props.currentInterlocutors.length !== 1 && (
                        <Box
                            sx={{
                                display: 'flex',
                                flexGrow: 1,
                                alignItems: 'center',
                            }}
                        >
                            {props.selectedChatRoom.type === RoomTypeEnum.DM ? (
                                <Avatar src={props.selectedChatRoom.image} sx={{ width: 40, height: 40, m: 2 }} />
                            ) : (
                                <GroupsRoundedIcon sx={{ color: 'gray', m: 2, marginLeft: 3 }} />
                            )}
                            <Typography variant="h6" color="inherit">
                                {props.selectedChatRoom.name}
                            </Typography>
                        </Box>
                    )}
                {props.selectedChatRoom !== undefined && props.selectedChatRoom?.roomId !== 0 ? (
                    <Box
                        sx={{
                            display: 'flex',
                            flexGrow: 1,
                            justifyContent: 'flex-end',
                            alignItems: 'center',
                        }}
                    >
                        <IconButton onClick={openRoomSettings}>
                            <MoreVertRoundedIcon sx={{ m: 2 }} />
                        </IconButton>
                        <Menu
                            id="chat-settings"
                            anchorEl={anchorEl}
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'right',
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            open={Boolean(anchorEl)}
                            onClose={closeRoomSettings}
                        >
                            <ChatRoomOptionsList
                                selectedChatRoom={props.selectedChatRoom}
                                closeRoomSettings={closeRoomSettings}
                                setSelectedChatRoom={props.setSelectedChatRoom}
                                fetchRooms={props.fetchRooms}
                            />
                        </Menu>
                    </Box>
                ) : (
                    <Box
                        sx={{
                            display: 'flex',
                            flexGrow: 1,
                            justifyContent: 'flex-end',
                            alignItems: 'center',
                        }}
                    >
                        <IconButton disabled>
                            <MoreVertRoundedIcon sx={{ m: 2 }} />
                        </IconButton>
                    </Box>
                )}
            </Box>
            {/* 2nd div -> where users' messages appear */}
            <Box
                sx={{
                    display: 'flex',
                    flexDirection: 'column-reverse',
                    flexGrow: 200,
                    borderTop: '1px solid',
                    borderColor: 'lightgray',
                    overflow: 'hidden',
                    overflowY: 'scroll',
                }}
            >
                {props.selectedChatRoom !== undefined && (
                    <List sx={{ p: 3 }}>
                        {props.convMessages.map((message, index) => {
                            if (
                                auth.blockedUsersId === null ||
                                auth.blockedUsersId.find((id) => id === message.senderId) === undefined
                            ) {
                                if (message.senderId === null)
                                    return <ConversationNotification key={index} message={message} />;
                                else
                                    return message.senderId === auth.userId ? (
                                        <MessageSent key={index} message={message} />
                                    ) : (
                                        <MessageReceived key={index} message={message} />
                                    );
                            }
                        })}
                    </List>
                )}
            </Box>
            {/* 3rd div -> where user type and send messages */}
            <Box sx={{ display: 'flex', alignItems: 'flex-end' }}>
                <FormControl fullWidth variant="outlined" sx={{ flexGrow: 1 }}>
                    <OutlinedInput
                        disabled={inputDisabled}
                        value={messageTyped}
                        onChange={(event) => {
                            setMessageTyped(event.target.value);
                        }}
                        onKeyDown={handleKeyDown}
                    />
                </FormControl>
            </Box>
        </Card>
    );
};

export default ChatRoom;
