import React, { useContext, useEffect, useState } from 'react';
import { Avatar, Box, Card, List, ListItem, ListItemAvatar, ListItemText } from '@mui/material';
import FormatListBulletedRoundedIcon from '@mui/icons-material/FormatListBulletedRounded';
import { styled } from '@mui/material/styles';
import InputBase from '@mui/material/InputBase';
import Fab from '@mui/material/Fab';
import AddIcon from '@mui/icons-material/Add';
import SearchIcon from '@mui/icons-material/Search';
import { createNewRoom } from '../../api/axios/chatRoutes';
import { RoomOverviewDto } from '../../common/chat/dto/room.dto.out';
import { AuthContext } from '../../contexts/auth.context';
import { startListeningInRoom } from '../../api/socketIo/chatMessages';
import NewChannelDialog from './NewChannelDialog';
import { RoomTypeEnum } from '../../common/chat/types/rooms.types';
import DiscoverableChannelsDialog from './DiscoverableChannelsDialog';
import GroupsRoundedIcon from '@mui/icons-material/GroupsRounded';

const Search = styled('div')(({ theme }) => ({
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: 'transparent',
    marginBlockStart: 10,
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: 'inherit',
    '& .MuiInputBase-input': {
        padding: theme.spacing(1, 1, 1, 0),
        paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    },
}));

export interface ChatRoomsListProps {
    chatRoomsList: RoomOverviewDto[];
    setChatRoomsList: (roomsOverview: RoomOverviewDto[]) => void;
    selectedChatRoom: RoomOverviewDto;
    loadSelectedChatRoomData: (roomOverview: RoomOverviewDto) => void;
    selectedIndex: number;
    fetchRooms: (selectedRoom: number) => void;
}

const ChatRoomsList = (props: ChatRoomsListProps): JSX.Element => {
    const auth = useContext(AuthContext);
    const [showNewChannelDialog, setShowNewChannelDialog] = useState<boolean>(false);
    const [showDiscoverableChannels, setShowDiscoverableChannels] = useState<boolean>(false);
    const [displayedChatRooms, setDisplayedChatRooms] = useState<RoomOverviewDto[]>(props.chatRoomsList);
    const [search, setSearch] = useState<string>('');

    useEffect(() => {
        if (props.chatRoomsList !== undefined) {
            if (search !== '') {
                const searchedChannels = props.chatRoomsList.filter((chatRoomOverview) =>
                    chatRoomOverview.name.startsWith(search),
                );
                setDisplayedChatRooms(searchedChannels);
            } else setDisplayedChatRooms(props.chatRoomsList);
        }
    }, [props.chatRoomsList, search]);

    const handleCreateChannel = async (
        name: string,
        imagePath: string,
        type: RoomTypeEnum,
        password: string,
    ): Promise<void> => {
        const ownerId = await auth.userId;
        const newRoom = await createNewRoom({
            ownerId,
            name,
            image: imagePath,
            typeId: type,
            password,
        });
        await props.fetchRooms(newRoom.roomId);
        startListeningInRoom(newRoom.roomId);
        setShowNewChannelDialog(false);
    };

    const setRoomData = async (index: number): Promise<void> => {
        props.loadSelectedChatRoomData(props.chatRoomsList[index]);
    };

    return (
        <Card
            sx={{
                display: 'flex',
                flex: 1,
                flexGrow: 1,
                borderRadius: 2,
                flexDirection: 'column',
                minWidth: '30px',
                maxWidth: '40vw',
                maxHeight: '90vh',
                overflow: 'hidden',
            }}
        >
            <Box
                sx={{
                    display: 'flex',
                    flexGrow: 1,
                    alignItems: 'center',
                }}
            >
                <Search>
                    <SearchIconWrapper>
                        <SearchIcon />
                    </SearchIconWrapper>
                    <StyledInputBase
                        placeholder="Search…"
                        inputProps={{ 'aria-label': 'search' }}
                        onChange={(event) => setSearch(event.target.value)}
                    />
                </Search>
            </Box>
            <Box
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    flexGrow: 20,
                    borderTop: '1px solid',
                    borderColor: 'lightgray',
                    overflow: 'hidden',
                    overflowY: 'scroll',
                }}
            >
                <List>
                    {displayedChatRooms.map((chatRoomOverview, index) => {
                        return chatRoomOverview.bannedIds?.find((bannedUserId) => bannedUserId === auth.userId) ||
                            (chatRoomOverview.type === RoomTypeEnum.DM &&
                                auth.blockedUsersId !== null &&
                                auth.blockedUsersId.find(
                                    (id) => id === chatRoomOverview.usersIds.find((id) => id !== auth.userId),
                                ) !== undefined) ? (
                            <ListItem
                                button
                                disabled
                                key={index}
                                sx={{ border: '1px solid', borderColor: 'lightgray', borderRadius: 1 }}
                                onClick={() => setRoomData(index)}
                                selected={index === props.selectedIndex}
                            >
                                <ListItemAvatar>
                                    <Avatar src={chatRoomOverview.image} />
                                </ListItemAvatar>
                                <ListItemText primary={chatRoomOverview.name} />
                            </ListItem>
                        ) : (
                            <ListItem
                                button
                                key={index}
                                sx={{ border: '1px solid', borderColor: 'lightgray', borderRadius: 1 }}
                                onClick={() => setRoomData(index)}
                                selected={index === props.selectedIndex}
                            >
                                <ListItemAvatar>
                                    {chatRoomOverview.type === RoomTypeEnum.DM ? (
                                        <Avatar src={chatRoomOverview.image} />
                                    ) : (
                                        <GroupsRoundedIcon sx={{ color: 'gray', marginBlockStart: 1, marginLeft: 1 }} />
                                    )}
                                </ListItemAvatar>
                                <ListItemText primary={chatRoomOverview.name} />
                            </ListItem>
                        );
                    })}
                </List>
            </Box>
            <Box
                sx={{
                    display: 'flex',
                    backgroundColor: 'transparent',
                    justifyContent: 'space-around',
                    paddingBottom: 1,
                }}
            >
                <Fab color="primary" aria-label="add" size="large" onClick={() => setShowNewChannelDialog(true)}>
                    <AddIcon />
                </Fab>
                <Fab color="primary" aria-label="add" size="large" onClick={() => setShowDiscoverableChannels(true)}>
                    <FormatListBulletedRoundedIcon />
                </Fab>
            </Box>
            <NewChannelDialog
                open={showNewChannelDialog}
                handleClose={() => {
                    setShowNewChannelDialog(false);
                }}
                handleValidate={handleCreateChannel}
            />
            <DiscoverableChannelsDialog
                open={showDiscoverableChannels}
                onClose={() => {
                    setShowDiscoverableChannels(false);
                }}
                chatRoomsList={props.chatRoomsList}
                fetchRooms={props.fetchRooms}
            />
        </Card>
    );
};

export default ChatRoomsList;
