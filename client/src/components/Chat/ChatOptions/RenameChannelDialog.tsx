import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, TextField } from '@mui/material';
import Button from '@mui/material/Button';
import React, { useState } from 'react';
import { changeRoomName } from '../../../api/axios/chatRoutes';
import { RoomOverviewDto } from '../../../common/chat/dto/room.dto.out';

interface RenameChannelDialogProps {
    open: boolean;
    onClose: () => void;
    selectedChatRoom: RoomOverviewDto;
    setSelectedChatRoom: (newRoom: RoomOverviewDto) => void;
    fetchRooms: (selectedRoom: number) => void;
}

const RenameChannelDialog = (props: RenameChannelDialogProps): JSX.Element => {
    const [newName, setNewName] = useState<string>('');
    const [error, setError] = useState<string>('');

    const handleCancel = () => {
        setError('');
        props.onClose();
    };

    const handleSubmit = async (): Promise<void> => {
        if (newName === '' || newName.length > 20 || newName.match(/[%<>\\$'"?!_&@(){}§€`£+=\-/:;.,]/)) {
            setNewName('');
            setError('Use of forbidden character');
        } else {
            await changeRoomName(newName, props.selectedChatRoom.roomId);
            setNewName('');
            setError('');
            props.onClose();
        }
    };

    return (
        <Dialog onClose={handleCancel} open={props.open}>
            <DialogTitle>Rename channel:</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    The channel&lsquo;s name should be at least 1 and at most 20 characters long, only letters and
                    numbers are authorized
                </DialogContentText>
                <TextField
                    autoFocus
                    margin="dense"
                    id="name"
                    label="New name"
                    fullWidth
                    variant="standard"
                    onChange={(event) => setNewName(event.target.value)}
                    helperText={error}
                    error={!!error}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={handleCancel} color="error">
                    Cancel
                </Button>
                <Button onClick={handleSubmit} color="success">
                    Submit
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default RenameChannelDialog;
