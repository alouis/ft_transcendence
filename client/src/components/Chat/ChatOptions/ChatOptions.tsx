import React, { useContext, useState, useMemo } from 'react';
import { RoomOverviewDto } from '../../../common/chat/dto/room.dto.out';
import AccountBoxRoundedIcon from '@mui/icons-material/AccountBoxRounded';
import SportsEsportsRoundedIcon from '@mui/icons-material/SportsEsportsRounded';
import CancelRoundedIcon from '@mui/icons-material/CancelRounded';
import { useNavigate } from 'react-router-dom';
import { Box, Divider, List, ListItem, ListItemIcon, ListItemText } from '@mui/material';
import { AuthContext } from '../../../contexts/auth.context';
import DefaultOptionsRoom from './DefaultOptionsRoom';
import AdminOptionsRoom from './AdminOptionsRoom';
import OwnerOptionsRoom from './OwnerOptionsRoom';
import ExitChannelDialog from './ExitChannelDialog';
import ExitToAppIcon from '@mui/icons-material/ExitToApp';
import { removeUserFromRoom } from '../../../api/axios/chatRoutes';
import { blockUser, stopListeningInRoom } from '../../../api/socketIo/chatMessages';
import { getCurrentGamePlaying, getLoginByUsername } from '../../../api/axios/profileRoutes';
import { DialogContext } from '../../../contexts/dialog.context';
import { FriendsContext } from '../../../contexts/friends.context';
import { joinGame } from '../../../api/socketIo/gameMessages';

interface ChatRoomOptionsListProps {
    selectedChatRoom: RoomOverviewDto;
    setSelectedChatRoom: (newRoom: RoomOverviewDto) => void;
    closeRoomSettings: () => void;
    fetchRooms: (selectedRoom: number) => void;
}

interface OptionsType {
    title: string;
    icon: JSX.Element;
    action: (login: string) => void;
    disabled?: boolean;
}

export function ChatRoomOptionsList(props: ChatRoomOptionsListProps) {
    const navigate = useNavigate();
    const dialogContext = useContext(DialogContext);
    const auth = useContext(AuthContext);
    const { friends } = useContext(FriendsContext);

    const [openExitChannel, setOpenExitChannel] = useState<boolean>(false);

    const inviteOption: OptionsType[] = useMemo((): OptionsType[] => {
        const opponentId = props.selectedChatRoom.usersIds?.find((id) => id != auth.userId);
        const opponent = friends.find((friend) => friend.id === opponentId);
        if (!opponent || !opponentId) return [];
        if (!opponent?.isActive)
            return [
                {
                    title: 'Play pong',
                    icon: <SportsEsportsRoundedIcon />,
                    action: () => {
                        dialogContext.setGameCreationDialogProps({ open: true, roomId: props.selectedChatRoom.roomId });
                    },
                    disabled: true,
                },
            ];
        else if (!opponent.isPlaying)
            return [
                {
                    title: 'Play Pong',
                    icon: <SportsEsportsRoundedIcon />,
                    action: () => {
                        dialogContext.setGameCreationDialogProps({ open: true, roomId: props.selectedChatRoom.roomId });
                    },
                },
            ];
        else
            return [
                {
                    title: 'Join Game',
                    icon: <SportsEsportsRoundedIcon />,
                    action: async () => {
                        const currentGameId = await getCurrentGamePlaying(opponentId);
                        joinGame({ gameId: currentGameId }, () => navigate(`/game/${currentGameId}`));
                    },
                },
            ];
    }, [friends]);

    const dmOptions: OptionsType[] = [
        {
            title: 'Profile',
            icon: <AccountBoxRoundedIcon />,
            action: (login: string) => {
                navigate(`/profile?friend-login=${login}`);
            },
        },
        ...inviteOption,
        {
            title: 'Block user',
            icon: <CancelRoundedIcon />,
            action: () => {
                const friendId = props.selectedChatRoom.usersIds.find((id) => id !== auth.userId);
                if (friendId !== undefined) blockUser(friendId, auth.refreshBlockedUsersId);
            },
        },
    ];

    const handleExitChannel = async (): Promise<void> => {
        if (props.selectedChatRoom !== undefined && props.selectedChatRoom.ownerId === auth.userId)
            setOpenExitChannel(true);
        else {
            await removeUserFromRoom(auth.userId, props.selectedChatRoom.roomId);
            stopListeningInRoom(props.selectedChatRoom.roomId);
            handleCloseExit();
        }
    };

    const handleDMOptionClick = async (option: OptionsType) => {
        const friendLogin = await getLoginByUsername(props.selectedChatRoom.name);
        props.closeRoomSettings();
        option.action(friendLogin);
    };

    const handleCloseExit = async (): Promise<void> => {
        setOpenExitChannel(false);
        props.closeRoomSettings();
    };

    return (
        <Box>
            {props.selectedChatRoom?.type === 1 ? (
                <List>
                    {dmOptions.map((option, index) => (
                        <ListItem
                            button
                            onClick={() => {
                                handleDMOptionClick(option);
                            }}
                            key={index}
                            disabled={option.disabled}
                        >
                            <ListItemIcon>{option.icon}</ListItemIcon>
                            <ListItemText primary={option.title} />
                        </ListItem>
                    ))}
                </List>
            ) : (
                <DefaultOptionsRoom
                    selectedChatRoom={props.selectedChatRoom}
                    setSelectedChatRoom={props.setSelectedChatRoom}
                    closeRoomSettings={props.closeRoomSettings}
                    fetchRooms={props.fetchRooms}
                />
            )}
            {props.selectedChatRoom?.type !== 1 &&
                props.selectedChatRoom?.adminsIds?.find((adminId) => adminId === auth.userId) && (
                    <Box>
                        <Divider />
                        <AdminOptionsRoom
                            selectedChatRoom={props.selectedChatRoom}
                            setSelectedChatRoom={props.setSelectedChatRoom}
                            closeRoomSettings={props.closeRoomSettings}
                            fetchRooms={props.fetchRooms}
                        />
                    </Box>
                )}
            {props.selectedChatRoom?.type !== 1 && props.selectedChatRoom?.ownerId === auth.userId && (
                <Box>
                    <Divider />
                    <OwnerOptionsRoom
                        selectedChatRoom={props.selectedChatRoom}
                        setSelectedChatRoom={props.setSelectedChatRoom}
                        closeRoomSettings={props.closeRoomSettings}
                        fetchRooms={props.fetchRooms}
                    />
                </Box>
            )}
            {props.selectedChatRoom?.type !== 1 && (
                <Box>
                    <Divider />
                    <ListItem
                        button
                        onClick={() => {
                            handleExitChannel();
                        }}
                    >
                        <ListItemIcon>
                            <ExitToAppIcon />
                        </ListItemIcon>
                        <ListItemText primary="Exit channel" />
                    </ListItem>
                    <ExitChannelDialog
                        open={openExitChannel}
                        onClose={handleCloseExit}
                        selectedChatRoom={props.selectedChatRoom}
                    />
                </Box>
            )}
        </Box>
    );
}
