import * as React from 'react';
import Snackbar from '@mui/material/Snackbar';
import Slide, { SlideProps } from '@mui/material/Slide';
import { Button } from '@mui/material';

interface NotificationSnackbarProps {
    open: boolean;
    setOpen: (open: boolean) => void;
    message?: string;
    action?: () => void;
    actionText?: string;
}
type TransitionProps = Omit<SlideProps, 'direction'>;

function TransitionUp(props: TransitionProps) {
    return <Slide {...props} direction="up" />;
}

const NotificationSnackbar = (props: NotificationSnackbarProps) => {
    const transition: React.ComponentType<TransitionProps> = TransitionUp;

    const handleClose = () => {
        props.setOpen(false);
    };

    return (
        <Snackbar
            open={props.open}
            onClose={handleClose}
            TransitionComponent={transition}
            message={props.message}
            action={
                props.action && (
                    <Button color="inherit" size="small" onClick={props.action}>
                        {props.actionText ?? 'Join'}
                    </Button>
                )
            }
            key={transition ? transition.name : ''}
        />
    );
};
export default NotificationSnackbar;
