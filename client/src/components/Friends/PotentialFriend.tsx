import { Avatar, Box, Card, CardActionArea, CardContent, IconButton, Typography } from '@mui/material';
import React, { useContext } from 'react';
import RemoveCircleOutlineRoundedIcon from '@mui/icons-material/RemoveCircleOutlineRounded';
import AddCircleOutlineRoundedIcon from '@mui/icons-material/AddCircleOutlineRounded';
import { addNewFriend } from '../../api/axios/friendsRoutes';
import { User } from '../../common/user/dto/user.dto.out';
import { useNavigate } from 'react-router-dom';
import { SnackbarContext } from '../../contexts/snackbar.context';

interface PotentialFriendProps {
    potentialFriend: User;
    loadData: () => void;
    hiddenPeopleIds: number[];
    setHiddenPeopleIds: (peopleIds: number[]) => void;
}

const PotentialFriend = (props: PotentialFriendProps): JSX.Element => {
    const navigate = useNavigate();
    const snackbarContext = useContext(SnackbarContext);

    const handleClick = () => {
        navigate(`/profile?friend-login=${props.potentialFriend.login}`);
    };

    const addFriend = async (): Promise<void> => {
        try {
            await addNewFriend(props.potentialFriend.login);
            await props.loadData();
        } catch (e) {
            snackbarContext.showSnackbar('Error while adding new friend', 'error');
        }
    };

    const hidePotentialFriend = async () => {
        props.setHiddenPeopleIds([...props.hiddenPeopleIds, props.potentialFriend.id]);
    };

    return (
        <Card sx={{ display: 'flex', m: 2, flexDirection: 'column', width: 150, maxHeight: 150 }}>
            <CardContent
                sx={{
                    display: 'flex',
                    flexGrow: 1,
                    flex: 1,
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'center',
                    overflow: 'hidden',
                }}
            >
                <CardActionArea
                    sx={{
                        display: 'flex',
                        flex: 1,
                        flexGrow: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        marginBlockStart: 1,
                        marginBlockEnd: 1,
                    }}
                    onClick={handleClick}
                >
                    <Avatar
                        alt="img"
                        src={props.potentialFriend.avatar}
                        sx={{ width: 70, height: 70, marginBlockStart: 1 }}
                    ></Avatar>
                    <Typography
                        variant="subtitle1"
                        sx={{
                            maxWidth: '100px',
                            overflow: 'hidden',
                            textOverflow: 'ellipsis',
                            whiteSpace: 'nowrap',
                        }}
                    >
                        {props.potentialFriend.username}
                    </Typography>
                </CardActionArea>
            </CardContent>
            <Box
                sx={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    pl: 1,
                    pr: 1,
                }}
            >
                <IconButton aria-label="add" onClick={addFriend}>
                    <AddCircleOutlineRoundedIcon color="success" sx={{ height: 30, width: 30 }} />
                </IconButton>
                <IconButton aria-label="hide" onClick={hidePotentialFriend}>
                    <RemoveCircleOutlineRoundedIcon
                        color="error"
                        sx={{ height: 30, width: 30 }}
                    ></RemoveCircleOutlineRoundedIcon>
                </IconButton>
            </Box>
        </Card>
    );
};

export default PotentialFriend;
