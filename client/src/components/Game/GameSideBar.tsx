import { Avatar, Box, Card, Fab, styled } from '@mui/material';
import React, { useContext } from 'react';
import AddIcon from '@mui/icons-material/Add';
import FormatListBulletedRoundedIcon from '@mui/icons-material/FormatListBulletedRounded';
import LogoutRoundedIcon from '@mui/icons-material/LogoutRounded';
import { useNavigate } from 'react-router-dom';
import { leaveGame } from '../../api/socketIo/gameMessages';
import { UserFromDBDto } from '../../common/user/dto/user.dto.out';
import GroupsRoundedIcon from '@mui/icons-material/GroupsRounded';
import { DialogContext } from '../../contexts/dialog.context';

export interface GameSideBarProps {
    openGameListDialog: () => void;
    gameId?: number;
    users: UserFromDBDto[];
}

export const GameSideBar = ({ openGameListDialog, gameId, users }: GameSideBarProps) => {
    const navigate = useNavigate();
    const dialogContext = useContext(DialogContext);
    const handleLeaveGame = () => {
        if (gameId) leaveGame(gameId);
        navigate('/game', { replace: true });
    };

    const Div = styled('div')(({ theme }) => ({
        ...theme.typography.button,
        backgroundColor: theme.palette.background.paper,
        color: 'gray',
        fontSize: 20,
        borderBlockEnd: '1px solid',
        borderBlockStart: '1px solid',
        borderColor: 'gray',
        lineHeight: 0.5,
        padding: theme.spacing(1),
        marginRight: 10,
    }));

    return (
        <div
            style={{
                display: 'flex',
                flex: 1,
                width: '100%',
                flexDirection: 'column',
            }}
        >
            <div style={{ flex: 1 }} />

            <div style={{ flex: 3, display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
                {!!users?.length && (
                    <Card
                        sx={{
                            flex: 1,
                            my: 1,
                            display: 'flex',
                            flexDirection: 'column',
                            justifyContent: 'space-around',
                        }}
                        elevation={5}
                    >
                        <Box
                            sx={{
                                flex: 1,
                                display: 'flex',
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'start',
                                boxShadow: 3,
                                borderRadius: 1,
                            }}
                        >
                            <GroupsRoundedIcon sx={{ m: 1, color: 'gray' }} />
                            <Div>Attendees</Div>
                        </Box>
                        <div
                            style={{
                                display: 'flex',
                                flex: 3,
                                flexDirection: 'row',
                                alignItems: 'center',
                                flexWrap: 'wrap',
                                padding: 4,
                            }}
                        >
                            {users
                                .sort((u1, u2) => u1.username.localeCompare(u2.username))
                                .map((user) => (
                                    <Avatar key={user.id} alt="Room image" src={user.avatar} sx={{ m: 1 }} />
                                ))}
                        </div>
                    </Card>
                )}
                <div style={{ flex: 1, display: 'flex', justifyContent: 'space-around', alignItems: 'end' }}>
                    {gameId ? (
                        <Fab
                            color="secondary"
                            aria-label="add"
                            size="medium"
                            onClick={() => handleLeaveGame()}
                            disabled={!gameId}
                        >
                            <LogoutRoundedIcon />
                        </Fab>
                    ) : (
                        <>
                            <Fab
                                color="primary"
                                aria-label="add"
                                size="medium"
                                onClick={() => dialogContext.setGameCreationDialogProps({ open: true })}
                            >
                                <AddIcon />
                            </Fab>
                            <Fab color="primary" aria-label="add" size="medium" onClick={() => openGameListDialog()}>
                                <FormatListBulletedRoundedIcon />
                            </Fab>
                        </>
                    )}
                </div>
            </div>
            <div style={{ flex: 1 }} />
        </div>
    );
};
