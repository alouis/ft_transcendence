import * as THREE from 'three';
import { gameConfig, GameMap } from '../../../common/game/types/game.types';
import { addGrass } from './grass';

const createSunsetLight = (scene: THREE.Scene) => {
    const hemiLight = new THREE.HemisphereLight(0xffffff, 0xffffff, 1.5);
    hemiLight.color.setHSL(0.1, 1, 0.95);
    hemiLight.groundColor.setHSL(31 / 360, 1, 0.66);
    hemiLight.position.set(-2, 40, 0);
    scene.add(hemiLight);

    // const hemiLightHelper = new THREE.HemisphereLightHelper(hemiLight, 10);
    // scene.add(hemiLightHelper);

    const dirLight = new THREE.DirectionalLight(0xffffff, 6);
    dirLight.color.setHSL(0.1, 1, 0.95);
    dirLight.position.set(10, 10, -20);
    dirLight.position.multiplyScalar(10);
    scene.add(dirLight);

    dirLight.castShadow = true;

    dirLight.shadow.mapSize.width = 2048;
    dirLight.shadow.mapSize.height = 2048;

    const d = 50;

    dirLight.shadow.camera.left = -d;
    dirLight.shadow.camera.right = d;
    dirLight.shadow.camera.top = d;
    dirLight.shadow.camera.bottom = -d;

    dirLight.shadow.camera.far = 3500;
    dirLight.shadow.bias = -0.0001;

    // const dirLightHelper = new THREE.DirectionalLightHelper(dirLight, 10);
    // scene.add(dirLightHelper);

    return () => {
        scene.remove(dirLight);
        scene.remove(hemiLight);
        dirLight.shadow.map.dispose();
    };
};

const createIndoorLight = (scene: THREE.Scene) => {
    const light = new THREE.PointLight('white', 200);
    light.position.set(0, 30, 10);
    // light.shadow.camera.left = -30;
    // light.shadow.camera.right = 30;
    // light.shadow.camera.top = 35;
    // light.shadow.camera.bottom = -30;

    light.castShadow = true;

    scene.add(light);

    // --- draw line in space to represent light's sources ---
    // const helper = new THREE.CameraHelper(light.shadow.camera);
    // scene.add(helper);
    return () => {
        scene.remove(light);
        light.shadow.map.dispose();
    };
};

export const initCamera = (camera: THREE.PerspectiveCamera, map: GameMap | undefined) => {
    const fov = 50;
    const relativeHeight = 50;
    const camZ = relativeHeight / 2 / Math.tan((fov / 2 / 180) * Math.PI);

    camera.position.z = camZ - 15;
    camera.position.z = map === GameMap.OUTDOOR ? camZ : camZ;
    camera.position.y = map === GameMap.OUTDOOR ? 10 : 20;
    return camera;
};

export const initRenderer = (renderer: THREE.WebGLRenderer, sceneWidth: number) => {
    renderer.toneMapping = THREE.ACESFilmicToneMapping;
    renderer.toneMappingExposure = 0.5;
    renderer.shadowMap.enabled = true;
    renderer.physicallyCorrectLights = true;
    renderer.setSize(sceneWidth, sceneWidth / gameConfig.displayRatio);
    // --- device’s pixel ratio is the difference factor between virtual pixels of virtual viewport & device’s physical pixels ---
    const pixelRatio = window.devicePixelRatio ? window.devicePixelRatio : 1;
    renderer.setPixelRatio(pixelRatio);
    return renderer;
};

const setHexahedronInScene = (
    scene: THREE.Scene,
    pos: THREE.Vector3,
    dimensions: THREE.Vector3,
    texturePath: string,
) => {
    const geometry = new THREE.BoxBufferGeometry(dimensions.x, dimensions.y, dimensions.z);
    const textureLoader = new THREE.TextureLoader();
    const texture = textureLoader.load(texturePath);
    const material = new THREE.MeshStandardMaterial({
        map: texture,
    });
    const cube = new THREE.Mesh(geometry, material);
    cube.position.set(pos.x, pos.y, pos.z);
    cube.castShadow = true;
    cube.receiveShadow = true;
    scene.add(cube);

    return () => {
        scene.remove(cube);
        geometry.dispose();
        material.dispose();
        texture.dispose();
    };
};

export const setBrickFloor = (scene: THREE.Scene, pos: THREE.Vector3, dimensions: THREE.Vector3) => {
    const textureLoader = new THREE.TextureLoader();
    const material = new THREE.MeshStandardMaterial({
        roughness: 0.7,
        color: 0xffffff,
        bumpScale: 0.02,
        metalness: 0.2,
    });
    const texture1 = textureLoader.load('/threejs/textures/brick_diffuse.jpg', function (map) {
        map.wrapS = THREE.RepeatWrapping;
        map.wrapT = THREE.RepeatWrapping;
        map.anisotropy = 4;
        map.repeat.set(1, 1);
        // map.encoding = THREE.sRGBEncoding;
        material.map = map;
        material.needsUpdate = true;
    });
    const texture2 = textureLoader.load('/threejs/textures/brick_bump.jpg', function (map) {
        map.wrapS = THREE.RepeatWrapping;
        map.wrapT = THREE.RepeatWrapping;
        map.anisotropy = 4;
        map.repeat.set(1, 1);
        material.bumpMap = map;
        material.needsUpdate = true;
    });

    const geometry = new THREE.BoxBufferGeometry(dimensions.x, dimensions.y, dimensions.z);
    const floor = new THREE.Mesh(geometry, material);
    floor.position.set(pos.x, pos.y, pos.z);
    floor.receiveShadow = true;
    scene.add(floor);

    return () => {
        scene.remove(floor);
        geometry.dispose();
        material.dispose();
        texture1.dispose();
        texture2.dispose();
    };
};

export const setWoodenCeiling = (scene: THREE.Scene, pos: THREE.Vector3, dimensions: THREE.Vector3) => {
    const material = new THREE.MeshStandardMaterial({
        roughness: 0.8,
        color: 0xffffff,
        metalness: 0.2,
        bumpScale: 0.0005,
    });
    const textureLoader = new THREE.TextureLoader();
    const texture1 = textureLoader.load('/threejs/textures/hardwood2_diffuse.jpg', function (map) {
        map.wrapS = THREE.RepeatWrapping;
        map.wrapT = THREE.RepeatWrapping;
        map.anisotropy = 4;
        map.repeat.set(10, 24);
        map.encoding = THREE.sRGBEncoding;
        material.map = map;
        material.needsUpdate = true;
    });
    const texture2 = textureLoader.load('/threejs/textures/hardwood2_bump.jpg', function (map) {
        map.wrapS = THREE.RepeatWrapping;
        map.wrapT = THREE.RepeatWrapping;
        map.anisotropy = 4;
        map.repeat.set(10, 24);
        material.bumpMap = map;
        material.needsUpdate = true;
    });
    const texture3 = textureLoader.load('/threejs/textures/hardwood2_roughness.jpg', function (map) {
        map.wrapS = THREE.RepeatWrapping;
        map.wrapT = THREE.RepeatWrapping;
        map.anisotropy = 4;
        map.repeat.set(10, 24);
        material.roughnessMap = map;
        material.needsUpdate = true;
    });

    const geometry = new THREE.BoxBufferGeometry(dimensions.x, dimensions.y, dimensions.z);
    const mesh = new THREE.Mesh(geometry, material);
    mesh.receiveShadow = true;
    mesh.rotation.x = -Math.PI;
    mesh.position.set(pos.x, pos.y, pos.z);
    scene.add(mesh);

    return () => {
        scene.remove(mesh);
        material.dispose();
        texture1.dispose();
        texture2.dispose();
        texture3.dispose();
        geometry.dispose();
    };
};

export const setBallInScene = (scene: THREE.Scene, texturePath: string) => {
    const geometry = new THREE.SphereGeometry(1);
    const clearcoatBallMap = new THREE.TextureLoader().load('/threejs/textures/clearcoatNormaMap.png');
    const texture = new THREE.TextureLoader().load(texturePath);
    const material = new THREE.MeshPhysicalMaterial({
        metalness: 0.0,
        roughness: 0.1,
        clearcoat: 1.0,
        normalMap: texture,
        clearcoatNormalMap: clearcoatBallMap,
        //
        // y scale is negated to compensate for normal map handedness.
        clearcoatNormalScale: new THREE.Vector2(2.0, -2.0),
    });
    const mesh = new THREE.Mesh(geometry, material);
    mesh.castShadow = true;
    scene.add(mesh);
    return { mesh, geometry, material, texture, clearcoatBallMap };
};

export const createOutdoorScene = (scene: THREE.Scene) => {
    scene.background = new THREE.Color('skyblue');

    const groundDimensions = new THREE.Vector3(215, 0.1, 215);
    const groundPos = new THREE.Vector3(0, 0, 10);
    const cleanGround = setHexahedronInScene(scene, groundPos, groundDimensions, '/threejs/textures/grass.jpg');

    const cleanLight = createSunsetLight(scene);
    const cleanGrass = addGrass(scene);
    const clean = () => {
        cleanGround();
        cleanGrass();
        cleanLight();
    };

    return clean;
};

export const createIndoorScene = (scene: THREE.Scene) => {
    scene.background = new THREE.Color('white');

    const floorDimensions = new THREE.Vector3(94, 0.1, 40);
    const floorPos = new THREE.Vector3(0, 0, 10);
    const cleanFloor = setBrickFloor(scene, floorPos, floorDimensions);

    const ceilingDimensions = new THREE.Vector3(94, 0.1, 40);
    const ceilingPos = new THREE.Vector3(0, 40, 10);
    const cleanCeiling = setWoodenCeiling(scene, ceilingPos, ceilingDimensions);

    const wallDimensions = new THREE.Vector3(94, 40, 0.1);
    const wallPos = new THREE.Vector3(0, 20, -10);
    const cleanWall = setHexahedronInScene(scene, wallPos, wallDimensions, '/threejs/textures/anticwall.png');

    const leftWallDimensions = new THREE.Vector3(0.1, 40, 40);
    const leftWallPos = new THREE.Vector3(-47, 20, 10);
    const cleanLeftWall = setHexahedronInScene(
        scene,
        leftWallPos,
        leftWallDimensions,
        '/threejs/textures/anticwallpainting.png',
    );

    const rightWallDimensions = new THREE.Vector3(0.1, 40, 40);
    const rightWallPos = new THREE.Vector3(47, 20, 10);
    const cleanRightWall = setHexahedronInScene(
        scene,
        rightWallPos,
        rightWallDimensions,
        '/threejs/textures/anticwallpainting2.png',
    );

    const cleanLight = createIndoorLight(scene);
    const clean = () => {
        cleanFloor();
        cleanCeiling();
        cleanLight();
        cleanWall();
        cleanLeftWall();
        cleanRightWall();
        // textureBackground.dispose();
    };
    return clean;
};
