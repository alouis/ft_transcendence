import { Box, Button, Card } from '@mui/material';
import React, { useState } from 'react';
import EmojiEventsRoundedIcon from '@mui/icons-material/EmojiEventsRounded';
import HeartBrokenRoundedIcon from '@mui/icons-material/HeartBrokenRounded';
import MilitaryTechRoundedIcon from '@mui/icons-material/MilitaryTechRounded';
import StarHalfRoundedIcon from '@mui/icons-material/StarHalfRounded';
import SportsTennisRoundedIcon from '@mui/icons-material/SportsTennisRounded';
import WorkspacePremiumRoundedIcon from '@mui/icons-material/WorkspacePremiumRounded';
import { styled } from '@mui/material/styles';
import TotalPointsScoredDialog from './Achievements/TotalPointsScoredDialog';
import GamesPlayedDialog from './Achievements/GamesPlayedDialog';
import { UserProfile } from '../../common/user/dto/user.dto.out';

const PlayerScores = (props: { userScores: UserProfile | undefined }): JSX.Element => {
    const [openPointsScored, setOpenPointsScored] = useState<boolean>(false);
    const [openGamesPlayed, setOpenGamesPlayed] = useState<boolean>(false);

    const Div = styled('div')(({ theme }) => ({
        ...theme.typography.button,
        backgroundColor: theme.palette.background.paper,
        color: 'gray',
        fontSize: 20,
        borderBlockStart: '0.5px solid',
        borderBlockEnd: '0.5px solid',
        marginBottom: 15,
        lineHeight: 0.5,
        padding: theme.spacing(1),
    }));

    const handleOpenPointsScored = () => {
        setOpenPointsScored(true);
    };

    const handleOpenGamesPlayed = () => {
        setOpenGamesPlayed(true);
    };

    const handleClose = () => {
        setOpenPointsScored(false);
        setOpenGamesPlayed(false);
    };

    return (
        <Card
            sx={{
                flexGrow: 1,
                flex: 4,
                padding: 0.1,
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                borderRadius: 2,
                m: 1,
            }}
        >
            <Box
                sx={{
                    flex: 1,
                    flexGrow: 1,
                    display: 'flex',
                    flexDirection: 'row',
                    marginBlockStart: 2,
                    marginRight: 2,
                    marginLeft: 2,
                }}
            >
                <Box
                    sx={{
                        flex: 1,
                        flexGrow: 1,
                        borderRight: 'solid',
                        borderColor: 'lightgray',
                        display: 'flex',
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                >
                    <Box
                        sx={{
                            flex: 1,
                            flexGrow: 1,
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center',
                            flexDirection: 'column',
                        }}
                    >
                        <EmojiEventsRoundedIcon sx={{ color: 'gray', height: 100, width: 100 }} />
                        <Div>{'Victories'}</Div>
                    </Box>
                    <Box sx={{ flex: 1, flexGrow: 1, fontSize: 80 }}>{props.userScores?.victories}</Box>
                </Box>
                <Box
                    sx={{
                        flex: 1,
                        flexGrow: 1,
                        display: 'flex',
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                >
                    <Box
                        sx={{
                            flex: 1,
                            flexGrow: 1,
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center',
                            flexDirection: 'column',
                        }}
                    >
                        <MilitaryTechRoundedIcon sx={{ color: 'gray', height: 100, width: 100 }} />
                        <Div>{'Ranking'}</Div>
                    </Box>
                    <Box sx={{ flex: 1, flexGrow: 1, fontSize: 80 }}>{props.userScores?.rank ?? '-'}</Box>
                </Box>
            </Box>
            <Box
                sx={{
                    flex: 1,
                    flexGrow: 1,
                    display: 'flex',
                    flexDirection: 'row',
                    marginBlockEnd: 2,
                    marginRight: 2,
                    marginLeft: 2,
                }}
            >
                <Box
                    sx={{
                        flex: 1,
                        flexGrow: 1,
                        borderBlockStart: 'solid',
                        borderColor: 'lightgray',
                    }}
                >
                    <Box
                        sx={{
                            flex: 1,
                            flexGrow: 1,
                            display: 'flex',
                            flexDirection: 'column',
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}
                    >
                        <Box
                            sx={{
                                flex: 1,
                                flexGrow: 1,
                                display: 'flex',
                                alignItems: 'center',
                                justifyContent: 'center',
                                flexDirection: 'column',
                            }}
                        >
                            <HeartBrokenRoundedIcon
                                sx={{ color: 'gray', height: 100, width: 100, marginBlockStart: 1 }}
                            />
                            <Div>{'Losses'}</Div>
                        </Box>
                        <Box sx={{ flex: 1, flexGrow: 1, fontSize: 80 }}>{props.userScores?.losses}</Box>
                    </Box>
                </Box>
                <Box
                    sx={{
                        flex: 1,
                        flexGrow: 1,
                        borderBlockStart: 'solid',
                        borderLeft: 'solid',
                        borderColor: 'lightgray',
                    }}
                >
                    <Box
                        sx={{
                            flex: 1,
                            flexGrow: 1,
                            display: 'flex',
                            flexDirection: 'column',
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}
                    >
                        <Box
                            sx={{
                                flex: 1,
                                flexGrow: 1,
                                display: 'flex',
                                alignItems: 'center',
                                justifyContent: 'center',
                                flexDirection: 'column',
                            }}
                        >
                            <StarHalfRoundedIcon
                                sx={{
                                    color: 'gray',
                                    height: 100,
                                    width: 100,
                                    marginBlockStart: 1,
                                }}
                            />
                            <Div>{'Achievements'}</Div>
                        </Box>
                        <Box
                            sx={{
                                flex: 1,
                                flexGrow: 1,
                                display: 'flex',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                                flexDirection: 'row',
                            }}
                        >
                            <Button
                                sx={{ color: 'black', m: 1, marginRight: 2, boxShadow: 1, borderRadius: 2, p: 2 }}
                                onClick={handleOpenPointsScored}
                            >
                                <SportsTennisRoundedIcon
                                    sx={{
                                        height: 50,
                                        width: 50,
                                    }}
                                />
                            </Button>
                            {props.userScores !== undefined && (
                                <TotalPointsScoredDialog
                                    userId={props.userScores.id}
                                    open={openPointsScored}
                                    onClose={handleClose}
                                />
                            )}
                            <Button
                                sx={{ color: 'black', m: 1, marginRight: 2, boxShadow: 1, borderRadius: 2, p: 2 }}
                                onClick={handleOpenGamesPlayed}
                            >
                                <WorkspacePremiumRoundedIcon
                                    sx={{
                                        height: 50,
                                        width: 50,
                                    }}
                                />
                            </Button>
                            {props.userScores !== undefined && (
                                <GamesPlayedDialog
                                    userId={props.userScores.id}
                                    open={openGamesPlayed}
                                    onClose={handleClose}
                                />
                            )}
                        </Box>
                    </Box>
                </Box>
            </Box>
        </Card>
    );
};

export default PlayerScores;
