import { Avatar, Box, Typography } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { MatchResult } from '../../common/game/dto/game.dto.out';

interface GameResultsCardProps {
    ownUsername: string | undefined;
    ownAvatar: string | undefined;
    matchResults: MatchResult;
}

const GameResultsCard = (props: GameResultsCardProps): JSX.Element => {
    const [userScoreColor, setUserScoreColor] = useState<string>('');
    const [opponentScoreColor, setOpponentScoreColor] = useState<string>('');

    useEffect(() => {
        if (props.matchResults.ownScore > props.matchResults.opponentScore) {
            setUserScoreColor('green');
            setOpponentScoreColor('red');
        } else if (props.matchResults.ownScore < props.matchResults.opponentScore) {
            setUserScoreColor('red');
            setOpponentScoreColor('green');
        } else {
            setUserScoreColor('orange');
            setOpponentScoreColor('orange');
        }
    }, []);

    return (
        <Box
            sx={{
                display: 'flex',
                flex: 1,
                flexGrow: 1,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                maxHeight: 50,
                minWidth: 300,
            }}
        >
            <Box
                sx={{
                    display: 'flex',
                    flexWrap: 'nowrap',
                    flex: 1,
                    flexGrow: 3,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'start',
                    m: 1,
                }}
            >
                <Avatar alt="img" src={props.ownAvatar} sx={{ width: 30, height: 30, m: 0.5 }} />
                <Typography
                    sx={{
                        m: 0.5,
                        maxWidth: '22vw',
                        overflow: 'hidden',
                        textOverflow: 'ellipsis',
                        whiteSpace: 'nowrap',
                    }}
                >
                    {props.ownUsername}
                </Typography>
            </Box>
            <Box
                sx={{
                    display: 'flex',
                    flex: 1,
                    flexGrow: 1,
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    m: 1,
                }}
            >
                <Typography sx={{ m: 0.5, color: userScoreColor, fontWeight: 'bold' }}>
                    {props.matchResults.ownScore == -1 ? 'X' : props.matchResults.ownScore}
                </Typography>
                <Typography sx={{ m: 0.5 }}>-</Typography>
                <Typography sx={{ m: 0.5, color: opponentScoreColor, fontWeight: 'bold' }}>
                    {props.matchResults.opponentScore == -1 ? 'X' : props.matchResults.opponentScore}
                </Typography>
            </Box>
            <Box
                sx={{
                    display: 'flex',
                    flex: 1,
                    flexGrow: 3,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'flex-end',
                    m: 1,
                }}
            >
                <Typography
                    sx={{
                        m: 0.5,
                        maxWidth: '22vw',
                        overflow: 'hidden',
                        textOverflow: 'ellipsis',
                        whiteSpace: 'nowrap',
                    }}
                >
                    {props.matchResults.opponentUsername}
                </Typography>
                <Avatar alt="img" src={props.matchResults.opponentAvatar} sx={{ width: 30, height: 30, m: 0.5 }} />
            </Box>
        </Box>
    );
};

export default GameResultsCard;
