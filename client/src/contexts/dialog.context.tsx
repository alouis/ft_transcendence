import React, { FC, useState } from 'react';
import { GameCreationDialog } from '../components/Game/GameCreationDialog';
import { GameResultDialog, ResultEnum } from '../components/Game/GameResultDialog';

interface DialogContextProps {
    setGameCreationDialogProps: React.Dispatch<React.SetStateAction<gameCreationDialogPropsType>>;
    setGameResultDialogProps: React.Dispatch<React.SetStateAction<gameResultDialogPropsType>>;
}

interface gameCreationDialogPropsType {
    open: boolean;
    roomId?: number;
}

interface gameResultDialogPropsType {
    open: boolean;
    result?: ResultEnum;
}

export const DialogContext = React.createContext<DialogContextProps>({} as DialogContextProps);

const DialogProvider: FC = ({ children }) => {
    const [gameCreationDialogProps, setGameCreationDialogProps] = useState<gameCreationDialogPropsType>({
        open: false,
    } as gameCreationDialogPropsType);
    const [gameResultDialogProps, setGameResultDialogProps] = useState<gameResultDialogPropsType>({
        open: false,
    } as gameResultDialogPropsType);

    return (
        <DialogContext.Provider
            value={{
                setGameCreationDialogProps,
                setGameResultDialogProps,
            }}
        >
            {children}
            <GameCreationDialog
                open={gameCreationDialogProps.open}
                onClose={() => setGameCreationDialogProps({ open: false })}
                roomId={gameCreationDialogProps.roomId}
            />
            <GameResultDialog
                open={gameResultDialogProps.open}
                onClose={() => setGameResultDialogProps({ open: false })}
                result={gameResultDialogProps.result}
            />
        </DialogContext.Provider>
    );
};

export default DialogProvider;
