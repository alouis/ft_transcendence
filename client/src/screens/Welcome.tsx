import React from 'react';
import Background from '../components/Background';
import { LogIn } from '../components/LogIn';
import AccessibilityNewRoundedIcon from '@mui/icons-material/AccessibilityNewRounded';
import { Box, Button, Typography } from '@mui/material';
import PublicIcon from '@mui/icons-material/Public';

const Welcome = (): JSX.Element => {
    return (
        <div>
            <Box sx={{ display: 'flex', flexDirection: 'column', m: 10, alignItems: 'center', height: '90vh' }}>
                <Box
                    sx={{
                        flex: 1,
                        display: 'flex',
                        flexDirection: 'row',
                        alignItems: 'center',
                        maxWidth: '600px',
                        flexWrap: 'wrap',
                    }}
                >
                    <AccessibilityNewRoundedIcon
                        sx={{ flex: 1, height: '200px', width: '200px', color: 'lightblue' }}
                    />
                    <PublicIcon sx={{ flex: 1, height: '150px', width: '150px', color: 'lightblue' }} />
                    <AccessibilityNewRoundedIcon
                        sx={{ flex: 1, height: '200px', width: '200px', color: 'lightblue' }}
                    />
                </Box>
                <Typography flex={1} sx={{ fontSize: '5vw', color: 'gray' }} align="center">
                    Transcendence Experience <br />
                    Welcome!
                </Typography>
                <LogIn />
                <Box sx={{ flex: 1 }}>
                    <Button
                        color="success"
                        variant="outlined"
                        onClick={() => {
                            window.open(
                                `https://api.intra.42.fr/oauth/authorize?client_id=${process.env.REACT_APP_TRANS_CLIENT_ID}&redirect_uri=http%3A%2F%2Flocalhost%3A3001%2Fprogress&response_type=code`,
                                '_self',
                            );
                        }}
                    >
                        SIGN IN WITH 42
                    </Button>
                </Box>
            </Box>
            <Background />
        </div>
    );
};

export default Welcome;
