import { Box, Card, Typography } from '@mui/material';
import React, { useContext, useEffect, useState } from 'react';
import { findPotentialFriends } from '../api/axios/friendsRoutes';
import FriendInList from '../components/Friends/FriendInList';
import PotentialFriend from '../components/Friends/PotentialFriend';
import { User } from '../common/user/dto/user.dto.out';
import { FriendsContext } from '../contexts/friends.context';

const Friends = (): JSX.Element => {
    const [potentialFriends, setPotentialFriends] = useState<User[]>([]);
    const friendsContext = useContext(FriendsContext);
    const [hiddenPeopleIds, setHiddenPeopleIds] = useState<number[]>([]);

    const loadPotentialFriends = async () => {
        try {
            const potentialFriendsFromDB = await findPotentialFriends();
            setPotentialFriends(potentialFriendsFromDB);
        } catch (e) {
            setPotentialFriends([]);
        }
    };

    const loadData = async (): Promise<void> => {
        try {
            await loadPotentialFriends();
            await friendsContext.loadFriendsList();
        } catch (e) {
            setPotentialFriends([]);
        }
    };

    useEffect(() => {
        loadPotentialFriends();
    }, [friendsContext.friends]);

    return (
        <Box sx={{ display: 'flex', flex: 1, flexGrow: 1, flexDirection: 'column' }}>
            <Card
                sx={{
                    display: 'flex',
                    flex: 1,
                    flexGrow: 1,
                    flexDirection: 'column',
                    p: 1,
                    maxHeight: '45vh',
                    overflow: 'hidden',
                    borderRadius: 2,
                    m: 1,
                }}
            >
                <Typography variant="h3" color="gray">
                    Your friends
                </Typography>

                <Card
                    sx={{
                        display: 'flex',
                        flexWrap: 'wrap',
                        flex: 1,
                        flexGrow: 1,
                        borderRadius: 2,
                        overflow: 'hidden',
                        overflowY: 'scroll',
                    }}
                >
                    {friendsContext.friends !== [] &&
                        friendsContext.friends.map((friend, index) => (
                            <FriendInList key={index} friend={friend} loadData={loadData} />
                        ))}
                </Card>
            </Card>
            <Card
                sx={{
                    display: 'flex',
                    flex: 1,
                    flexGrow: 1,
                    flexDirection: 'column',
                    p: 1,
                    maxHeight: '45vh',
                    overflow: 'hidden',
                    borderRadius: 2,
                    m: 1,
                }}
            >
                <Typography variant="h3" color="gray">
                    Meet others
                </Typography>

                <Card
                    sx={{
                        display: 'flex',
                        flexWrap: 'wrap',
                        flex: 1,
                        flexGrow: 1,
                        borderRadius: 2,
                        overflow: 'hidden',
                        overflowY: 'scroll',
                    }}
                >
                    {potentialFriends !== [] &&
                        potentialFriends?.map((potentialFriend, index) => {
                            if (
                                potentialFriend.username !== null &&
                                hiddenPeopleIds.find((hiddenPersonId) => hiddenPersonId === potentialFriend.id) ===
                                    undefined
                            )
                                return (
                                    <PotentialFriend
                                        key={index}
                                        potentialFriend={potentialFriend}
                                        loadData={loadData}
                                        hiddenPeopleIds={hiddenPeopleIds}
                                        setHiddenPeopleIds={setHiddenPeopleIds}
                                    />
                                );
                        })}
                </Card>
            </Card>
        </Box>
    );
};

export default Friends;
