import { Box } from '@mui/material';
import React, { useContext, useEffect, useRef, useState } from 'react';
import { findDMRoomForUser, findRoomsForUser } from '../api/axios/chatRoutes';
import { chatSocket } from '../api/socketIo';
import { MessageInConversation } from '../common/chat/dto/messages.dto.in';
import { RoomOverviewDto } from '../common/chat/dto/room.dto.out';
import ChatRoom from '../components/Chat/ChatRoom';
import ChatRoomsList from '../components/Chat/ChatRoomsList';
import { retrieveConversation } from '../api/axios/chatRoutes';
import { useSearchParams } from 'react-router-dom';
import { FriendsContext } from '../contexts/friends.context';
import { User } from '../common/user/dto/user.dto.out';
import NotificationSnackbar from '../components/NotificationSnackbar';
import { AuthContext } from '../contexts/auth.context';
import { startListeningInRoom } from '../api/socketIo/chatMessages';
import { RoomTypeEnum } from '../common/chat/types/rooms.types';

const Chat = (): JSX.Element => {
    const [chatRoomsList, setChatRoomsList] = useState<RoomOverviewDto[]>([]);
    const [selectedChatRoom, setSelectedChatRoom] = useState<RoomOverviewDto>(chatRoomsList?.[0]);
    const [convMessages, setConvMessages] = useState<MessageInConversation[]>([]);
    const [currentInterlocutors, setCurrentInterlocutors] = useState<User[]>([]);
    const [searchParams] = useSearchParams();
    const refConvMessage = useRef(convMessages);
    const friendsContext = useContext(FriendsContext);
    const refSelectedChatRoom = useRef(selectedChatRoom);
    const [openNotifAlert, setOpenNotifAlert] = useState<boolean>(false);
    const [notification, setNotification] = useState<{ message: string; action?: () => void; actionText?: string }>();
    const auth = useContext(AuthContext);
    const emptyChatRoom: RoomOverviewDto = {
        roomId: 0,
        type: 0,
        name: '',
        image: '',
        usersIds: [],
        adminsIds: [],
        ownerId: 0,
        mutedIds: null,
        bannedIds: null,
    };

    const selectedIndex =
        selectedChatRoom?.roomId !== 0
            ? chatRoomsList?.findIndex((chatRoom) => chatRoom.roomId === selectedChatRoom?.roomId)
            : -1;

    const loadConversation = async (roomId: number): Promise<void> => {
        const conversation = await retrieveConversation(roomId);
        setConvMessages(conversation);
    };

    const loadSelectedChatRoomUsers = async (roomUsersId: number[]): Promise<void> => {
        const selectedChatRoomUsers = friendsContext.friends.filter((friend) =>
            roomUsersId?.find((userId) => userId === friend.id),
        );
        setCurrentInterlocutors(selectedChatRoomUsers);
    };

    const loadSelectedChatRoomData = async (roomOverview: RoomOverviewDto): Promise<void> => {
        setSelectedChatRoom(roomOverview);
        loadConversation(roomOverview.roomId).catch(() => {
            return;
        });
        loadSelectedChatRoomUsers(roomOverview.usersIds);
    };

    const setCurrentInterlocutorFromRoute = async (friendLogin: string): Promise<void> => {
        const roomsOverview = await findRoomsForUser();
        setChatRoomsList(roomsOverview);
        const roomOverview = await findDMRoomForUser(friendLogin);
        await loadSelectedChatRoomData(roomOverview);
        startListeningInRoom(roomOverview.roomId);
    };

    const loadChatRoomsList = async (): Promise<void> => {
        const roomsOverview = await findRoomsForUser();
        setChatRoomsList(roomsOverview);
    };

    const loadData = async (friendLogin: string | null) => {
        try {
            if (friendLogin !== null && !(await auth.isUserBlocked(friendLogin)))
                await setCurrentInterlocutorFromRoute(friendLogin);
            else await loadChatRoomsList();
        } catch (e) {
            setChatRoomsList([]);
            setSelectedChatRoom(emptyChatRoom);
            setConvMessages([]);
            setCurrentInterlocutors([]);
            manageNotification('A critical error happened');
        }
    };

    useEffect(() => {
        const fetch = async () => {
            const friendLogin = searchParams.get('friend-login');
            await loadData(friendLogin);
            await fetchRooms(refSelectedChatRoom.current?.roomId);
        };
        fetch().catch(() => {
            return;
        });
    }, [friendsContext.friends]);

    useEffect(() => {
        chatSocket.on('notification', (notification: string) => {
            manageNotification(notification);
        });
        chatSocket.on('join-room', (roomId: number) => {
            startListeningInRoom(roomId);
        });
        chatSocket.on('modification', () => {
            fetchRooms(refSelectedChatRoom.current?.roomId);
        });
        return () => {
            chatSocket.off('notification');
            chatSocket.off('join-room');
            chatSocket.off('modification');
        };
    }, []);

    useEffect(() => {
        chatSocket.on('events', (messageReceived: MessageInConversation) => {
            if (
                selectedChatRoom !== undefined &&
                selectedChatRoom !== emptyChatRoom &&
                messageReceived.roomId === selectedChatRoom.roomId
            )
                setConvMessages([...refConvMessage.current, messageReceived]);
        });
        return () => {
            chatSocket.off('events');
        };
    }, [selectedChatRoom]);

    useEffect(() => {
        refConvMessage.current = convMessages;
        refSelectedChatRoom.current = selectedChatRoom;
    }, [convMessages, selectedChatRoom]);

    useEffect(() => {
        loadSelectedChatRoomUsers(refSelectedChatRoom.current?.usersIds);
    }, [friendsContext.friends]);

    useEffect(() => {
        if (selectedChatRoom !== undefined) {
            if (selectedChatRoom.type !== RoomTypeEnum.DM) loadConversation(selectedChatRoom?.roomId);
            else {
                setSelectedChatRoom(emptyChatRoom);
                setConvMessages([]);
            }
        }
    }, [auth.blockedUsersId]);

    const fetchRooms = async (selectedRoomId: number): Promise<void> => {
        try {
            const roomsOverview = await findRoomsForUser();
            setChatRoomsList(roomsOverview);
            const newSelectedRoom = roomsOverview.find((room) => room.roomId === selectedRoomId);
            if (
                newSelectedRoom != undefined &&
                newSelectedRoom?.bannedIds?.find((bannedUserId) => bannedUserId === auth.userId) === undefined
            )
                await loadSelectedChatRoomData(newSelectedRoom);
            else {
                setSelectedChatRoom(emptyChatRoom);
                setConvMessages([]);
            }
        } catch (e) {
            setSelectedChatRoom(emptyChatRoom);
            setConvMessages([]);
        }
    };

    const manageNotification = async (
        notification: string,
        action?: () => void,
        actionText?: string,
    ): Promise<void> => {
        setNotification({ message: notification, action, actionText });
        setOpenNotifAlert(true);
        await fetchRooms(refSelectedChatRoom.current?.roomId);
    };

    return (
        <Box
            sx={{
                flexGrow: 1,
                flex: 1,
                padding: 0.1,
                display: 'flex',
                flexDirection: 'row',
            }}
        >
            <Box sx={{ flex: 1, display: 'flex' }}>
                <ChatRoomsList
                    chatRoomsList={chatRoomsList}
                    setChatRoomsList={setChatRoomsList}
                    selectedChatRoom={selectedChatRoom}
                    loadSelectedChatRoomData={loadSelectedChatRoomData}
                    selectedIndex={selectedIndex}
                    fetchRooms={fetchRooms}
                />
            </Box>
            <Box sx={{ flex: 2.5, display: 'flex' }}>
                <ChatRoom
                    selectedChatRoom={selectedChatRoom}
                    setSelectedChatRoom={setSelectedChatRoom}
                    convMessages={convMessages}
                    currentInterlocutors={currentInterlocutors}
                    fetchRooms={fetchRooms}
                />
            </Box>
            <NotificationSnackbar
                open={openNotifAlert}
                setOpen={setOpenNotifAlert}
                message={notification?.message}
                action={notification?.action}
                actionText={notification?.actionText}
            />
        </Box>
    );
};

export default Chat;
