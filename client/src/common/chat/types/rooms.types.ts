export enum RoomTypeEnum {
    'PUBLIC' = 3,
    'PRIVATE' = 2,
    'PROTECTED' = 4,
    'DM' = 1,
}
