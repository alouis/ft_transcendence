import clientAPI from '.';
import { MessageInConversation } from '../../common/chat/dto/messages.dto.in';
import { CreateRoomDto } from '../../common/chat/dto/room.dto.in';
import { RoomOverviewDto } from '../../common/chat/dto/room.dto.out';
import { User } from '../../common/user/dto/user.dto.out';

export const createNewRoom = async (room: CreateRoomDto | null): Promise<RoomOverviewDto> => {
    if (room === null) {
        room = {
            ownerId: 2,
            name: 'roomTest',
            image: undefined,
            typeId: 4,
            password: 'plain password 123 !',
        };
        const roomOverview = await clientAPI.post('chat/new-room', { room });
        return roomOverview.data;
    }
    const roomOverview = await clientAPI.post('chat/new-room', { room });
    return roomOverview.data;
};

export const findRoomsForUser = async (): Promise<RoomOverviewDto[]> => {
    const roomsOverview = await clientAPI.get('chat/rooms');
    return roomsOverview.data;
};

export const findDMRoomForUser = async (friendLogin: string): Promise<RoomOverviewDto> => {
    const dmRoom = await clientAPI.get(`chat/dm-room/${friendLogin}`);
    return dmRoom.data;
};

export const deleteChat = async (roomId: number): Promise<void> => {
    await clientAPI.post(`chat/delete-room/${roomId}`);
};

export const retrieveConversation = async (roomId: number): Promise<MessageInConversation[]> => {
    const conversation = await clientAPI.get(`chat/conversation/${roomId}`);
    return conversation.data;
};

export const changeRoomName = async (roomName: string, roomId: number): Promise<void> => {
    const data = {
        roomName: roomName,
        roomId: roomId,
    };
    await clientAPI.post(`chat/room-name`, { data });
};

export const addUserToRoom = async (userId: number, roomId: number): Promise<void> => {
    const data = {
        userId: userId,
        roomId: roomId,
    };
    await clientAPI.post('chat/user-in', { data });
};

export const removeUserFromRoom = async (userId: number, roomId: number): Promise<void> => {
    const data = {
        userId: userId,
        roomId: roomId,
    };
    await clientAPI.post('chat/user-out', { data });
};

export const findUsersForRoom = async (roomId: number): Promise<User[]> => {
    const users = await clientAPI.get(`chat/users/${roomId}`);
    return users.data;
};

export const addAdminToRoom = async (userId: number, roomId: number): Promise<void> => {
    const data = {
        userId: userId,
        roomId: roomId,
    };
    await clientAPI.post('chat/add-admin', { data });
};

export const removeAdminFromRoom = async (userId: number, roomId: number): Promise<void> => {
    const data = {
        userId: userId,
        roomId: roomId,
    };
    await clientAPI.post('chat/remove-admin', { data });
};

export const muteUserForRoom = async (userId: number, roomId: number, duration: number): Promise<void> => {
    const data = {
        userId: userId,
        roomId: roomId,
        duration: duration,
    };
    await clientAPI.post('chat/mute', { data });
};

export const banUserFromRoom = async (userId: number, roomId: number, duration: number): Promise<void> => {
    const data = {
        userId: userId,
        roomId: roomId,
        duration: duration,
    };
    await clientAPI.post('chat/ban', { data });
};

export const findPotentialOwners = async (roomId: number): Promise<User[] | null> => {
    const potentialOwners = await clientAPI.get(`chat/owners/${roomId}`);
    return potentialOwners.data;
};

export const changeOwnerOfRoom = async (userId: number, roomId: number): Promise<void> => {
    const data = {
        userId: userId,
        roomId: roomId,
    };
    await clientAPI.post('chat/change-owner', { data });
};

export const deleteChannel = async (roomId: number): Promise<void> => {
    await clientAPI.post(`chat/delete-room/${roomId}`);
};

export const findDiscoverableChannels = async (): Promise<RoomOverviewDto[]> => {
    const discoverableChannels = await clientAPI.get('chat/discoverable-rooms');
    return discoverableChannels.data;
};

export const joinRoom = async (roomId: number, password: string | null): Promise<boolean> => {
    const data = {
        roomId,
        password,
    };

    const isRoomJoined = await clientAPI.post('chat/join-room', { data });
    return isRoomJoined.data;
};

export const isRoomProtected = async (roomId: number): Promise<boolean> => {
    const roomIsProtected = await clientAPI.get(`chat/room-type/${roomId}`);
    if (roomIsProtected.data === 4) return true;
    else return false;
};

export const newPasswordForRoom = async (roomId: number, password: string): Promise<boolean> => {
    const data = {
        roomId,
        password,
    };
    const newPasswordStored = await clientAPI.post('chat/new-password', { data });
    return newPasswordStored.data;
};

export const updateChannelType = async (roomId: number, newType: number): Promise<void> => {
    const data = {
        roomId,
        newType,
    };
    await clientAPI.post('chat/update-type', { data });
};
