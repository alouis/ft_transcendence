import axios from 'axios';
import { API_PORT } from '../../common/network';

const clientAPI = axios.create({
    baseURL: `http://localhost:${API_PORT}`,
    timeout: 10000,
    // headers: {
    //     'Access-Control-Allow-Origin': '*',
    // },
});

clientAPI.interceptors.request.use(
    (config) => {
        if (config.headers && typeof document.cookie?.split('=')[1] !== 'undefined')
            config.headers.authorization = 'Bearer ' + document.cookie?.split('=')[1];
        return config;
    },
    (error) => {
        // console.log('Error in request');
        // console.trace();
        return Promise.reject(error);
    },
);
// Add a response interceptor
clientAPI.interceptors.response.use(
    (response) => {
        // Any status code that lie within the range of 2xx cause this function to trigger
        // Do something with response data
        return response;
    },
    function (error) {
        // Any status codes that falls outside the range of 2xx cause this function to trigger
        // Do something with response error
        // console.log('Error in response');
        // console.trace();
        return Promise.reject(error);
    },
);

export default clientAPI;
