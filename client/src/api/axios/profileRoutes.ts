import clientAPI from '.';
import { MatchResult } from '../../common/game/dto/game.dto.out';
import { User, UserProfile } from '../../common/user/dto/user.dto.out';

export const find42UserLoginFromURLCode = async (code: string): Promise<string> => {
    const response = await clientAPI.get(`/auth/${code}`);
    return response.data;
};

export const generateAndStoreOTPSecret = async (login: string): Promise<string> => {
    const response = await clientAPI.post(`/2fa/generate/store/${login}`, { responseType: 'arraybuffer' });
    return response.data;
};

export const is2FAEnabled = async (login: string): Promise<boolean> => {
    const response = await clientAPI.get(`/2fa/enabled/${login}`);
    return response.data;
};

export const remove2FA = async (login: string) => {
    await clientAPI.post(`/2fa/disable/${login}`);
};

export const isCodeValid = async (login: string, code: string): Promise<boolean> => {
    const response = await clientAPI.post(`/2fa/validate/${login}/${code}`);
    return response.data;
};

export const generateJWT = async (login: string): Promise<string> => {
    const token = await clientAPI.get(`/auth/jwt/generate/${login}`);
    return token.data;
};

export const getLoginFromJWT = async (token: string): Promise<string> => {
    const login = await clientAPI.get(`auth/jwt/validate/${token}`);
    return login.data;
};

export const getUser = async (): Promise<User> => {
    const user = await clientAPI.get(`users`);
    return user.data;
};

export const getUserById = async (userId: number): Promise<User> => {
    const user = await clientAPI.get(`users/${userId}`);
    return user.data;
};

export const getCurrentGamePlaying = async (userId: number): Promise<number> => {
    const gameId = await clientAPI.get(`/users/current-game/${userId})`);
    return parseInt(gameId.data);
};

export const storeUniqueUsername = async (username: string): Promise<boolean> => {
    const usernameIsUnique = await clientAPI.post(`users/store/${username}`);
    return usernameIsUnique.data;
};

export const uploadAvatar = async (avatarFile: FormData) => {
    const headers = {
        headers: {
            'Content-Type': 'multipart/form-data; boundary :****************',
        },
    };
    const res = await clientAPI.post(`users/avatar/upload`, avatarFile, headers);
    return res.data;
};

export const deleteUploadedAvatar = async (filePath: string) => {
    const filename = filePath.split('/').pop();
    const res = await clientAPI.post(`users/avatar/delete/${filename}`);
    return res.data;
};

export const generateRandomAvatar = async (): Promise<string> => {
    const filePath = await clientAPI.get('users/avatar/random');
    return filePath.data;
};

export const storeUserAvatar = async (filePath: string): Promise<boolean> => {
    if (filePath === null) return false;
    const filename = filePath.split('/').pop();
    const res = await clientAPI.post(`users/avatar/store/${filename}`);
    return res.data.status === '200' ? true : false;
};

export const getUserAvatar = async (): Promise<string> => {
    const avatar = await clientAPI.get(`users/avatar`);
    return avatar.data;
};

export const getUserLoginById = async (userId: number): Promise<string> => {
    const userLogin = await clientAPI.get(`users/login/${userId}`);
    return userLogin.data;
};

export const getUserIdByLogin = async (login: string): Promise<number> => {
    const user = await clientAPI.get(`users/id/${login}`);
    return user.data;
};

export const getLoginByUsername = async (username: string): Promise<string> => {
    const userLogin = await clientAPI.get(`users/login/username/${username}`);
    return userLogin.data;
};

export const getUserProfile = async (login: string): Promise<UserProfile> => {
    // try catch if user doesnt exist
    const userProfile = await clientAPI.get(`users/profile/${login}`);
    return userProfile.data;
};

export const getBlockedUsersId = async (): Promise<number[] | null> => {
    const blockedUsersId = await clientAPI.get(`users/blocked-users`);
    return blockedUsersId.data === '' ? null : blockedUsersId.data;
};

export const getMatchHistory = async (userId: number): Promise<MatchResult[] | null> => {
    const matchHistory = await clientAPI.get(`users/match-history/${userId}`);
    return matchHistory.data === '' ? null : matchHistory.data;
};

export const getTotalPointsScored = async (userId: number): Promise<number> => {
    const totalPoints = await clientAPI.get(`users/total-points/${userId}`);
    return totalPoints.data;
};

export const getTotalGamesPlayed = async (userId: number): Promise<number> => {
    const totalGames = await clientAPI.get(`users/total-games/${userId}`);
    return totalGames.data;
};
