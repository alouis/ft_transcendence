import { gameSocket } from '.';
import {
    Attendee,
    GameMessage,
    GameSettings,
    GameUpdate,
    MoveDirection,
    TickMessage,
} from '../../common/game/types/game.types';
import { UserFromDBDto } from '../../common/user/dto/user.dto.out';

export const startListeningToGameSocket = (
    onGameTick: (message: TickMessage) => void,
    onGameUpdate: (data: { update: GameUpdate; attendees: UserFromDBDto[] }) => void,
    onAttendeeArrived: (attendee: Attendee) => void,
    onAttendeeLeft: (attendee: Attendee) => void,
) => {
    gameSocket.on(GameMessage.GAME_TICK, onGameTick);
    gameSocket.on(GameMessage.UPDATE, onGameUpdate);
    gameSocket.on(GameMessage.ATTENDEE_ARRIVED, onAttendeeArrived);
    gameSocket.on(GameMessage.ATTENDEE_LEFT, onAttendeeLeft);
};

export const createGame = (
    { level, map, powerups }: GameSettings,
    onGameCreated: (info: { gameId: number; error?: string }) => void,
) => {
    const socket = gameSocket.emit(GameMessage.CREATE_GAME, { level, map, powerups }, onGameCreated);
    if (!socket || !socket.connected) throw new Error('socket not connected');
};

export const joinGame = (
    { gameId }: { gameId: number },
    onGameJoined: (info: { gameId: number; error?: string }) => void,
) => {
    const socket = gameSocket.emit(GameMessage.JOIN_GAME, { gameId }, onGameJoined);
    if (!socket || !socket.connected) throw new Error('socket not connected');
};
export const leaveGame = (gameId: number) => {
    gameSocket.emit(GameMessage.LEAVE_GAME, { gameId });
};

export const stopListeningToGameSocket = (): void => {
    gameSocket.off(GameMessage.GAME_TICK);
    gameSocket.off(GameMessage.UPDATE);
    gameSocket.off(GameMessage.ATTENDEE_ARRIVED);
    gameSocket.off(GameMessage.ATTENDEE_LEFT);
};

export const move = (gameId: number, direction: MoveDirection): void => {
    gameSocket.emit(GameMessage.MOVE, { gameId, direction });
};

export const launchBall = (gameId: number): void => {
    gameSocket.emit(GameMessage.LAUNCH_BALL, { gameId });
};
