import { chatSocket } from '.';

export const sendMessage = (roomId: number, message: string): void => {
    chatSocket.emit('events', { roomId: roomId, message: message });
};

export const joinAllRooms = (): void => {
    chatSocket.emit('join-all-rooms');
};

export const sendGameInvite = (roomId: number, gameId: number): void => {
    chatSocket.emit('invite', { roomId, gameId });
};

export const stopListeningInRoom = (roomId: number): void => {
    chatSocket.emit('leave-room', { roomId });
};

export const startListeningInRoom = (roomId: number): void => {
    chatSocket.emit('join-room', { roomId });
};

export const blockUser = (blockedUserId: number, refreshBlockedUsersId: () => void): void => {
    chatSocket.emit('block-user', { blockedUserId }, (response: boolean) => {
        if (response === true) refreshBlockedUsersId();
    });
};

export const unblockUser = (blockedUserId: number, refreshBlockedUsersId: () => void) => {
    chatSocket.emit('unblock-user', { blockedUserId }, (response: boolean) => {
        if (response === true) refreshBlockedUsersId();
    });
};
